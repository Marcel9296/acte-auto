module.exports = (db) => {
  'use strict';
  const async = require("async");
  const {success: rhs} = require('./requestHandler');
  const _ = require('lodash');
  const Promise = require("node-promise").Promise;
  const fs = require('fs');

  function getExtension(file) {
    return file.originalname.split('.').pop().toLowerCase();
  }

  function checkFormula(cod, definitions) {
    for (let i = 0; i < definitions.length; i++) {
      if (definitions[i].formula) {
        let fm = definitions[i].formula.split(' ');
        if (fm.filter((m) => {
            return !isNaN(m) && parseInt(m) === cod;
          }).length) {
          definitions[i].id_definition_draft = definitions[i].id;
          definitions[i] = makeOperation(fm, definitions, definitions[i]);
          checkFormula(definitions[i].code_row, definitions);
        }
      }
    }
  }

  function makeOperation(fm, definitions, def) {
    let total_local = 0, total_more = 0;
    for (let j = 0; j < fm.length; j++) {
      if (!isNaN(fm[j])) {
        let df = definitions.filter((f) => {
          return f.code_row === parseInt(fm[j]);
        });
        if (df.length && df[0].area_local) {
          total_local += parseFloat(df[0].area_local);
        }
        if (df.length && df[0].area_more) {
          total_more += parseFloat(df[0].area_more);
        }
      }
    }
    def.area_local = total_local.toFixed(2);
    def.area_more = total_more.toFixed(2);
    return def;
  }

  function saveError(user, action, err, res) {
    console.log(action, err);
    if (user) {
      db.LogError.create({
        id_user: user.id,
        action: action,
        error: err.toString()
      }).then(() => {
        res.status(500);
        res.end();
      }).catch(() => {
        res.status(500);
        res.end();
      });
    }
  }

  function logAction(idUser, action, details) {
    if (idUser) {
      db.UserAction.create({id_user: idUser, action: action, details: details, date: new Date()}).then(() => {
      }).catch((err) => {
        console.log('update user action', err);
      });
    }
  }

  function similar(a, b) {
    let equivalency = 0,
      minLength = (a.length > b.length) ? b.length : a.length,
      maxLength = (a.length < b.length) ? b.length : a.length;
    for (let i = 0; i < minLength; i++) {
      if (a[i] === b[i]) {
        equivalency++;
      }
    }
    let weight = equivalency / maxLength;
    return (weight * 100);
  }

  function checkFormula3(cod, definitions) {
    for (let i = 0; i < definitions.length; i++) {
      if (definitions[i].formula) {
        let fm = definitions[i].formula.split(' ');
        if (fm.filter((m) => {
            return !isNaN(m) && parseInt(m) === cod;
          }).length) {
          definitions[i] = makeOperation3(fm, definitions, definitions[i]);
          checkFormula3(definitions[i].code_row, definitions);
        }
      }
    }
  }

  function makeOperation3(fm, definitions, def) {
    let total = {ha: 0, ari: 0, area: 0};
    for (let j = 0; j < fm.length; j++) {
      if (!isNaN(fm[j])) {
        let df = definitions.filter((f) => {
          return f.code_row === parseInt(fm[j]);
        });
        if (df.length) {
          for (let col in total) {
            if (total.hasOwnProperty(col) && df[0].hasOwnProperty(col)) {
              total[col] = eval((fm[j - 1] ? total[col] + fm[j - 1] : '') + (df[0][col] ? parseFloat(df[0][col]) : 0));
            }
          }
        }
      }
    }
    def.area = total.area.toFixed(2);
    return def;
  }

  function calculus2aTo3(req) {
    let d = new Promise(), asyncTasks = [], response = {};

    asyncTasks.push((callback) => {
      db.sequelize.query('SELECT dd.id, dd.code_row, dd.formula, c.area_local FROM "DefinitionDraft" dd ' +
      'inner join "SubChapterDraft" scd on dd.id_sub_chapter_draft = scd.id and scd.name = \'Cap II a\' ' +
      'inner join "ChapterDraft" cd on cd.id = scd.id_chapter_draft and cd.year = ' + req.user.currentYear + ' ' +
      'left join "Cap2a" c on c.id_definition_draft = dd.id and c.id_farm = ' + req.body.idFarm + ' ' +
      'where dd.code_row = 10 order by dd.code_row').then((resp) => {
        response.cap2a = resp[0].length ? resp[0][0] : {};
        callback();
      }).catch((err) => {
        callback(err);
      });
    });

    asyncTasks.push((callback) => {
      db.sequelize.query('SELECT dd.id, dd.code_row, dd.formula, c.area FROM "DefinitionDraft" dd ' +
      'inner join "SubChapterDraft" scd on dd.id_sub_chapter_draft = scd.id and scd.name = \'Cap III\' ' +
      'inner join "ChapterDraft" cd on cd.id = scd.id_chapter_draft and cd.year = ' + req.user.currentYear + ' ' +
      'left join "Cap3" c on c.id_definition_draft = dd.id and c.id_farm = ' + req.body.idFarm + ' ' +
      'order by dd.code_row').then((resp) => {
        response.defCap3 = resp[0];
        db.sequelize.query('delete from "Cap3" where id_farm = ' + req.body.idFarm + ' and id_definition_draft in (' + response.defCap3[0].id + ", " + response.defCap3[16].id + ')').then(() => {
          callback();
        }).catch((err) => {
          callback(err);
        });
      }).catch((err) => {
        callback(err);
      });
    });

    async.parallel(asyncTasks, (err) => {
      if (err) {
        d.reject(err);
      } else {
        response.defCap3[0].area = response.cap2a.area_local ? response.cap2a.area_local : 0;
        checkFormula3(1, response.defCap3);

        db.Cap3.bulkCreate([{
          area: response.defCap3[0].area,
          id_definition_draft: response.defCap3[0].id,
          id_farm: req.body.idFarm
        }, {
          area: response.defCap3[16].area,
          id_definition_draft: response.defCap3[16].id,
          id_farm: req.body.idFarm
        }]).then(() => {
          d.resolve(response);
        }).catch((err) => {
          d.reject(err);
        });
      }
    });
    return d;
  }

  function calculus2bTo2a(req, idFarm) {
    let d = new Promise(), asyncTasks = [], response = {};

    asyncTasks.push((callback) => {
      db.sequelize.query('SELECT dd.id, dd.code_row, dd.formula, c.area_more FROM "DefinitionDraft" dd ' +
      'inner join "SubChapterDraft" scd on dd.id_sub_chapter_draft = scd.id and scd.name = \'Cap II a\' ' +
      'inner join "ChapterDraft" cd on cd.id = scd.id_chapter_draft and cd.year = ' + req.user.currentYear + ' ' +
      'left join "Cap2a" c on c.id_definition_draft = dd.id and c.id_farm = ' + idFarm + ' ' +
      'order by dd.code_row').then((resp) => {
        db.sequelize.query('delete from "Cap2a" where id_farm = ' + idFarm).then(() => {
          response.definitions = resp[0];
          callback();
        }).catch((err) => {
          callback(err);
        });
      }).catch((err) => {
        callback(err);
      });
    });

    asyncTasks.push((callback) => {
      db.sequelize.query('select area_urban, area_extravilan, id_definition_draft, id_village from "Cap2b" where id_farm = ' + idFarm + ' and sale_date is null').then((resp) => {
        response.cap2b = resp[0];
        callback();
      }).catch((err) => {
        callback(err);
      });
    });

    asyncTasks.push((callback) => {
      db.sequelize.query('SELECT dd.id, dd.code_row, dd.formula, c.area FROM "DefinitionDraft" dd ' +
      'inner join "SubChapterDraft" scd on dd.id_sub_chapter_draft = scd.id and scd.name = \'Cap III\' ' +
      'inner join "ChapterDraft" cd on cd.id = scd.id_chapter_draft and cd.year = ' + req.user.currentYear + ' ' +
      'left join "Cap3" c on c.id_definition_draft = dd.id and c.id_farm = ' + idFarm + ' ' +
      'order by dd.code_row').then((resp) => {
        response.defCap3 = resp[0];
        db.sequelize.query('delete from "Cap3" where id_farm = ' + idFarm + ' and id_definition_draft in (' + response.defCap3[0].id + ", " + response.defCap3[16].id + ')').then(() => {
          callback();
        }).catch((err) => {
          callback(err);
        });
      }).catch((err) => {
        callback(err);
      });
    });

    asyncTasks.push((callback) => {
      db.sequelize.query('select v.id from "Unit" u ' +
      'left join "Address" a on a.id = u.id_address ' +
      'left join "Locality" l on a.id_locality = l.id ' +
      'left join "Village" v on v.id_locality = l.id ' +
      'where u.id = ' + req.user.id_unit).then((resp) => {
        response.idsUnit = _.map(resp[0], (r)=> {
          return r.id;
        });
        callback();
      }).catch((err) => {
        callback(err);
      });
    });

    async.parallel(asyncTasks, (err) => {
      if (err) {
        d.reject(err);
      } else {
        let index = 0, idDeff = 0;
        for (let i = 0; i < response.definitions.length; i++) {
          if (response.definitions[i].code_row === 11) {
            index = i;
            idDeff = response.definitions[i].id;
          }
          if (!response.definitions[i].formula && req.user.unit.cap2a_local) {
            delete response.definitions[i].area_local;
          }
          if (!response.definitions[i].formula && req.user.unit.cap2a_more) {
            delete response.definitions[i].area_more;
          }
          let cap2b = _.filter(response.cap2b, (r) => {
            return r.id_definition_draft === response.definitions[i].id;
          });

          if (cap2b.length) {
            let area = 0;
            let area_more = 0;
            for (let j = 0; j < cap2b.length; j++) {
              if (response.idsUnit.indexOf(cap2b[j].id_village) !== -1) {
                area += !!parseFloat(cap2b[j].area_urban) ? parseFloat(cap2b[j].area_urban) : 0;
                area += !!parseFloat(cap2b[j].area_extravilan) ? parseFloat(cap2b[j].area_extravilan) : 0;
              } else {
                area_more += !!parseFloat(cap2b[j].area_urban) ? parseFloat(cap2b[j].area_urban) : 0;
                area_more += !!parseFloat(cap2b[j].area_extravilan) ? parseFloat(cap2b[j].area_extravilan) : 0;
              }
            }
            if (req.user.unit.cap2a_local) {
              response.definitions[i].area_local = area.toFixed(2);
            }
            if (req.user.unit.cap2a_more) {
              response.definitions[i].area_more = area_more.toFixed(2);
            }
            response.definitions[i].id_definition_draft = response.definitions[i].id;
            if (response.definitions[i].code_row === 12) {
              response.definitions[index].id_definition_draft = idDeff;
              if (response.definitions[index].area_local) {
                response.definitions[index].area_local = parseFloat(response.definitions[index].area_local) + parseFloat(response.definitions[i].area_local);
              } else {
                response.definitions[index].area_local = response.definitions[i].area_local;
              }
              if (response.definitions[index].area_more) {
                response.definitions[index].area_more = parseFloat(response.definitions[index].area_more) + parseFloat(response.definitions[i].area_more);
              } else {
                response.definitions[index].area_more = response.definitions[i].area_more;
              }
              checkFormula(11, response.definitions);
            } else {
              checkFormula(response.definitions[i].code_row, response.definitions);
            }
          }
          if (response.definitions[i].area_local || response.definitions[i].area_more) {
            response.definitions[i].id_definition_draft = response.definitions[i].id;
          }
          if (response.definitions[i].code_row === 10) {
            response.defCap3[0].area = response.definitions[i].area_local;
          }
        }
        checkFormula3(1, response.defCap3);
        let asyncTasks1 = [];

        asyncTasks1.push((cb) => {
          db.Cap3.bulkCreate([{
            area: response.defCap3[0].area,
            id_definition_draft: response.defCap3[0].id,
            id_farm: idFarm
          }, {
            area: response.defCap3[16].area,
            id_definition_draft: response.defCap3[16].id,
            id_farm: idFarm
          }]).then(() => {
            cb();
          }).catch((err) => {
            cb(err);
          });
        });

        asyncTasks1.push((cb) => {
          db.Cap2a.bulkCreate(_.filter(response.definitions, (r) => {
            r.id_farm = idFarm;
            delete r.id;
            return r.id_definition_draft;
          })).then(() => {
            cb();
          }).catch((err) => {
            cb(err);
          });
        });

        async.parallel(asyncTasks1, (err) => {
          if (err) {
            d.reject(err);
          } else {
            d.resolve(response);
          }
        });
      }
    });
    return d;
  }

  function getFilesRecursive(folder, extension, handbook) {
    let fileContents = fs.readdirSync(folder), ln = extension.length + 1;
    fileContents.forEach(fileName => {
      if (fs.lstatSync(folder + '/' + fileName).isDirectory()) {
        getFilesRecursive(folder + '/' + fileName, extension, handbook);
      } else {
        if (((fileName.indexOf('.') !== 0) && (fileName.slice(-ln) === '.' + extension))) {
          handbook.push(fileName.substring(0, fileName.length - ln));
        }
      }
    });
  }

  return {
    searchSimilar: (val, arr, percent) => {
      let ok = true;
      if (val) {
        for (var i = 0; i < arr.length; i++) {
          if (similar(val, arr[i]) >= percent) {
            ok = false;
            break;
          }
        }
      } else {
        ok = false;
      }
      return ok;
    },

    customSort: (str, linkField) => {
      let resp = [];
      let arr = _.filter(str, (f) => {
        return f[linkField] === null;
      });
      if (arr.length) {
        for (let i = 0; i < arr.length; i++) {
          resp.push(arr[i]);
          let tmp = _.filter(str, (f) => {
            return f[linkField] === arr[i].id;
          });
          if (tmp.length) {
            resp = resp.concat(tmp);
          }
        }
        for (let i = 0; i < str.length; i++) {
          let f = resp.find((e) => {
            return e[linkField] === str[i][linkField];
          });
          if (!f) {
            resp.push(str[i]);
          }
        }
      } else {
        return str;
      }
      return resp;
    },

    verifyUnit: (req, res, next) => {
      req.app.locals.db.sequelize.query('select id_unit from "Farm" where id = ' + req.params.id_farm).then((resp) => {
        if (resp[0].length) {
          if (resp[0][0].id_unit === req.user.id_unit) {
            return next();
          } else {
            return res.status(403).json({
              success: false,
              message: 'Not autorized - bad request.'
            });
          }
        }
        next();
      }).catch((err) => {
        return res.status(403).json({
          success: false,
          message: 'Not autorized - error.' + err
        });
      });
    },

    multiPurposeUpdate: (req, res, table) => {
      let asyncTasks = [], start = new Date();

      if (req.body.toDelete.length) {
        let ids = table === 'Cap2b' ? _.map(req.body.toDelete, 'id') : req.body.toDelete;
        if (req.user.unit.make_history && table === 'Cap2b') {
          let history = [];
          for (let i = 0, ln = req.body.toDelete.length; i < ln; i++) {
            let cap = req.body.toDelete[i];
            history.push({visible: true, acquisition: cap.acquisition, action: 'ștergere', id_unit: req.user.id_unit, id_user: req.user.id, id_farm: cap.id_farm, details: cap.details});
          }
          asyncTasks.push(callback => {
            db.HistoryFarm.bulkCreate(history).then(() => callback()).catch((err) => callback(err));
          });
        }

        asyncTasks.push((callback) => {
          db.sequelize.query('delete from "' + table + '" where id in (' + ids + ')').then(() => callback()).catch((err) => callback(err));
        });
      }

      if (req.body.toCreate.length) {
        asyncTasks.push((callback) => {
          db[table].bulkCreate(req.body.toCreate).then(() => callback()).catch((err) => callback(err));
        });
        if (req.user.unit.make_history && table === 'Cap2b') {
          let history = [];
          for (let i = 0, ln = req.body.toCreate.length; i < ln; i++) {
            let cap = req.body.toCreate[i];
            history.push({visible: true, acquisition: cap.acquisition, action: 'adaugare', id_unit: req.user.id_unit, id_user: req.user.id, id_farm: cap.id_farm, details: cap.details});
          }
          asyncTasks.push(callback => {
            db.HistoryFarm.bulkCreate(history).then(() => callback()).catch((err) => callback(err));
          });
        }
      }

      if (req.body.toUpdate.length) {
        req.body.toUpdate.forEach(cap => {
          asyncTasks.push(callback => {
            db[table].update(cap, {where: {id: cap.id}}).then(() => callback()).catch(err => callback(err));
          });
          if (req.user.unit.make_history && table === 'Cap2b') {
            asyncTasks.push(callback => {
              db.HistoryFarm.create({
                visible: true, acquisition: cap.acquisition, action: 'modificare', id_unit: req.user.id_unit, id_user: req.user.id, id_farm: cap.id_farm,
                details: cap.details
              }).then(() => {
                callback();
              }).catch(err => callback(err));
            });
          }
        });
      }

      async.parallel(asyncTasks, (err) => {
        if (err) {
          saveError(req.user, 'update ' + table, err, res);
        } else {

          if (table === 'Cap2a' && !req.user.unit.cap2a_local) {
            calculus2aTo3(req).then(() => {
              rhs(res);
              logAction(req.user.id, 'Modificare ' + table + ' - ' + (new Date() - start) + ' ms', 'Gospodaria: ' + (req.body.farmName ? req.body.farmName : '') + ', create: ' + req.body.toCreate.length + ', update: ' + req.body.toUpdate.length +
              ', delete: ' + req.body.toDelete.length);
            }, err => saveError(req.user, 'update calcul Cap2a', err, res));
          } else if (table === 'Cap2b' && (req.user.unit.cap2a_local || req.user.unit.cap2a_more)) {
            calculus2bTo2a(req, req.body.id_farm).then(() => {
              rhs(res);
              logAction(req.user.id, 'Modificare ' + table + ' - ' + (new Date() - start) + ' ms', 'Gospodaria: ' + (req.body.farmName ? req.body.farmName : '') + ', create: ' + req.body.toCreate.length + ', update: ' + req.body.toUpdate.length +
              ', delete: ' + req.body.toDelete.length);
            }, err => saveError(req.user, 'update calcul Cap2b', err, res));
          } else {
            rhs(res);
            logAction(req.user.id, 'Modificare ' + table + ' - ' + (new Date() - start) + ' ms', 'Gospodaria: ' + (req.body.farmName ? req.body.farmName : '') + ', create: ' + req.body.toCreate.length + ', update: ' + req.body.toUpdate.length +
            ', delete: ' + req.body.toDelete.length);
          }
        }
      });
    },

    calculus2bTo2a: calculus2bTo2a,
    calculus2aTo3: calculus2aTo3,
    saveError: saveError,

    replaceDiacritice: (text) => {
      text = text.replace(new RegExp('ă', 'g'), 'a');
      text = text.replace(new RegExp('Ă', 'g'), 'A');
      text = text.replace(new RegExp('â', 'g'), 'a');
      text = text.replace(new RegExp('Â', 'g'), 'A');
      text = text.replace(new RegExp('î', 'g'), 'i');
      text = text.replace(new RegExp('Î', 'g'), 'I');
      text = text.replace(new RegExp('ş', 'g'), 's');
      text = text.replace(new RegExp('ș', 'g'), 's');
      text = text.replace(new RegExp('Ş', 'g'), 'S');
      text = text.replace(new RegExp('Ș', 'g'), 'S');
      text = text.replace(new RegExp('ț', 'g'), 't');
      text = text.replace(new RegExp('ţ', 'g'), 't');
      text = text.replace(new RegExp('Ţ', 'g'), 'T');
      return text;
    },

    getFilesRecursive: getFilesRecursive,

    validateSize: (file) => {
      return file.size <= 5242880;
    },

    validatePdf: (file) => {
      return getExtension(file) === 'pdf';
    },

    logAction: logAction,

    randomString: (len, charSet) => {
      charSet = charSet || 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
      var randomString = '';
      for (let i = 0; i < len; i++) {
        let randomPoz = Math.floor(Math.random() * charSet.length);
        randomString += charSet.substring(randomPoz, randomPoz + 1);
      }
      return randomString;
    },

    calcul15To3: (req, table, idFarm, idFarmTo) => {
      let d = new Promise(), asyncTasks = [], response = {};
      //let code_rows = (table === 'Cap15a' ? [3, 10]:[6, 13]);
      idFarm = parseInt(idFarm);
      idFarmTo = (idFarmTo && idFarm !== idFarmTo) ? parseInt(idFarmTo) : null;
      let idFarmsCalc = [idFarm];
      if (idFarmTo) {
        idFarmsCalc.push(idFarmTo);
      }

      asyncTasks.push((callback) => {
        db.sequelize.query('SELECT dd.id, dd.code_row, dd.formula, c.id_farm, c.area FROM "DefinitionDraft" dd ' +
        'inner join "SubChapterDraft" scd on dd.id_sub_chapter_draft = scd.id and scd.name = \'Cap III\' ' +
        'inner join "ChapterDraft" cd on cd.id = scd.id_chapter_draft and cd.year = ' + req.user.currentYear + ' ' +
        'left join "Cap3" c on c.id_definition_draft = dd.id and c.id_farm = ' + idFarm + ' ' +
        'order by dd.code_row').then((resp) => {
          response.defCap3 = resp[0];
          db.sequelize.query('delete from "Cap3" where id_farm = ' + idFarm).then(() => {
            callback();
          }).catch((err) => {
            callback(err);
          });
        }).catch((err) => {
          callback(err);
        });
      });

      if (idFarmTo) {
        asyncTasks.push((callback) => {
          db.sequelize.query('SELECT dd.id, dd.code_row, dd.formula, c.id_farm, c.area FROM "DefinitionDraft" dd ' +
          'inner join "SubChapterDraft" scd on dd.id_sub_chapter_draft = scd.id and scd.name = \'Cap III\' ' +
          'inner join "ChapterDraft" cd on cd.id = scd.id_chapter_draft and cd.year = ' + req.user.currentYear + ' ' +
          'left join "Cap3" c on c.id_definition_draft = dd.id and c.id_farm = ' + idFarmTo + ' ' +
          'order by dd.code_row').then((resp) => {
            response.defCap3To = resp[0];
            db.sequelize.query('delete from "Cap3" where id_farm = ' + idFarmTo).then(() => {
              callback();
            }).catch((err) => {
              callback(err);
            });
          }).catch((err) => {
            callback(err);
          });
        });
      }

      asyncTasks.push((callback) => {
        db.sequelize.query('select c.person_type, c.id_farm, sum(cf.area) as area from "' + table + '" c ' +
        'left join "Contract" co on co.id = c.id_contract ' +
        'left join "ContractField" cf on co.id = cf.id_contract ' +
        'where c.id_farm in (' + idFarmsCalc + ') ' +
        'group by c.person_type, c.id_farm').then((resp) => {
          response.cap15 = resp[0];
          callback();
        }).catch((err) => {
          callback(err);
        });
      });

      async.parallel(asyncTasks, (err) => {
        if (err) {
          d.reject(err);
        } else {
          let cap3 = [];

          for (let i = 0; i < response.defCap3.length; i++) {
            if (((response.defCap3[i].code_row === 10 || response.defCap3[i].code_row === 3) && table === 'Cap15a') || ((response.defCap3[i].code_row === 13 || response.defCap3[i].code_row === 6) &&
              table === 'Cap15b')) {
              response.defCap3[i].area = null;
              if (idFarmTo) {
                response.defCap3To[i].area = null;
              }

              for (let j = 0; j < response.cap15.length; j++) {
                if (response.cap15[j].id_farm === idFarm) {
                  if (((response.defCap3[i].code_row === 10 && response.cap15[j].person_type === 'Arendaş') || (response.defCap3[i].code_row === 13 && response.cap15[j].person_type === 'Concesionar')) ||
                    ((response.defCap3[i].code_row === 3 && response.cap15[j].person_type === 'Arendator') || (response.defCap3[i].code_row === 6 && response.cap15[j].person_type === 'Concedent'))) {
                    response.defCap3[i].area = (response.cap15[j].area * 100).toFixed(2);
                    checkFormula3(response.defCap3[i].code_row, response.defCap3);
                    //break;
                  }
                } else if (idFarmTo && response.cap15[j].id_farm === idFarmTo) {
                  if (((response.defCap3To[i].code_row === 10 && response.cap15[j].person_type === 'Arendaş') || (response.defCap3To[i].code_row === 13 && response.cap15[j].person_type === 'Concesionar')) ||
                    ((response.defCap3To[i].code_row === 3 && response.cap15[j].person_type === 'Arendator') || (response.defCap3To[i].code_row === 6 && response.cap15[j].person_type === 'Concedent'))) {
                    response.defCap3To[i].area = (response.cap15[j].area * 100).toFixed(2);
                    checkFormula3(response.defCap3To[i].code_row, response.defCap3To);
                    //break;
                  }
                }
              }

              if (!response.defCap3[i].area) {
                checkFormula3(response.defCap3[i].code_row, response.defCap3);
              }
              if (idFarmTo && !response.defCap3To[i].area) {
                checkFormula3(response.defCap3To[i].code_row, response.defCap3To);
              }
            }
          }

          for (let i = 0; i < response.defCap3.length; i++) {
            if (!!response.defCap3[i].area && parseFloat(response.defCap3[i].area) !== 0) {
              cap3.push({
                id_farm: idFarm,
                area: response.defCap3[i].area,
                id_definition_draft: response.defCap3[i].id
              });
            }
            if (idFarmTo && !!response.defCap3To[i].area && parseFloat(response.defCap3To[i].area) !== 0) {
              cap3.push({
                id_farm: idFarmTo,
                area: response.defCap3To[i].area,
                id_definition_draft: response.defCap3To[i].id
              });
            }
          }

          db.Cap3.bulkCreate(cap3).then(()=> {
            d.resolve(response);
          }).catch((err)=> {
            d.reject(err);
          });
        }
      });
      return d;
    }


  };
};
