(function () {
    'use strict';
    module.exports = function initJWTRoutes(app) {
        let authenticate = require('./authentication').authenticate,
            jwt = require('jsonwebtoken'),
            router = require('express').Router();
        const _ = require('lodash');

        router.post('/', function authRoute(req, res) {
            app.locals.db.sequelize.query(`select us.*, u.name, u.cui, con.name as county_name, vil.name as village_name, adr.id_county, adr.id_village, adr.id_locality, adr.street, adr.number, u.phone
                from "User" us
                left join "Unit" u on u.id = us.id_unit
                left join "Address" adr on adr.id = u.id_address
                left join "County" con on con.id = adr.id_county 
                left join "Locality" l on l.id = adr.id_locality 
                left join "Village" vil on vil.id = adr.id_village 
                where us.email = '${req.body.email}'`).then(resp => {
                let user = (resp[0].length) ? resp[0][0] : {};
                if (!user.id) {
                    res.render('login', {
                        success: false,
                        message: 'Auentificare eşuată. Utilizator inexistent'
                    });
                } else if (user) {
                    if (authenticate(req.body.password, user.salt, user.password)) {
                        if (user.active) {
                            delete user.salt;
                            delete user.password;
                            user.currentYear = user.currentYear ? user.currentYear:new Date().getFullYear();
                            let obj = user;
                            if (obj.unit) {
                                obj.unit.address = obj.address;
                                delete obj.address;
                            }
                            let token = jwt.sign(obj, global.config.sKey, {expiresIn: 86400});
                            obj.token = token;
                            if (_.includes([config.roles.superAdmin, config.roles.admin], user.role)) {
                                res.render('admin', {bootstrappedUser: obj});
                            } else if (_.includes([config.roles.clientAdmin, config.roles.client], user.role)) {
                                res.render('client', {bootstrappedUser: obj});
                            } else {
                                res.render('login', {
                                    success: true,
                                    message: 'Auentificare eşuată. Rol inexistent',
                                    token: token
                                });
                            }
                            // app.locals.db.sequelize.query('Select id, activity from "User" where id = ' + user.id).then((resp) => {
                            //     if (resp[0].length) {
                            //         let user = resp[0][0];
                            //         if (!user.activity) {
                            //             user.activity = {log: []};
                            //         }
                            //         if (user.activity.log.length > 30) {
                            //             user.activity.log = user.activity.log.slice(1);
                            //         }
                            //         user.activity.log.push({data: new Date().toISOString()});
                            //         app.locals.db.User.update({
                            //             last_login: new Date(),
                            //             activity: user.activity
                            //         }, {where: {id: user.id}});
                            //     }
                            // }).catch((err) => {
                            //     console.log('update last login & activity', err);
                            // });
                        } else {
                            res.render('login', {
                                success: false,
                                message: 'Contul dumneavostră este dezactivat'
                            });
                        }
                    } else {
                        res.render('login', {
                            success: false,
                            message: 'Parolă introdusă este greşită'
                        });
                    }
                }
            }).catch((err) => {
                console.log('find user jwt init', err);
            });
        });

        router.get('/secret', function test(req, res) {
            res.json(req.user);
        });

        return router;
    };
}());
