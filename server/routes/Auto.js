module.exports = app => {
    'use strict';

    let express = require('express'),
        ctrl = require('../controllers/autoCtrl')(app.locals.db),
        router = express.Router();

    router.post('/', ctrl.create);
    router.get('/:id', ctrl.find);
    router.get('/all/:id_person', ctrl.findAll);
    router.put('/', ctrl.update);
    router.delete('/:id', ctrl.destroy);



    return router;
};