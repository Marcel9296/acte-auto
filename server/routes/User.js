module.exports = app => {
	'use strict';

	let express = require('express'),
		ctrl = require('../controllers/userCtrl')(app.locals.db),
    auth = require('../utils/authentication'),
		router = express.Router();

	router.post('/admin', auth.requireLogin, ctrl.createFromAdmin);
	router.post('/client', auth.requireLogin, ctrl.createFromClient);

	router.get('/admin', auth.requireLogin, ctrl.findAllFromAdmin);
	router.get('/client', auth.requireLogin, ctrl.findAllFromClient);

	router.get('/admin/:id', auth.requireLogin, ctrl.findFromAdmin);
	router.get('/client/:id/:year', auth.requireLogin, ctrl.findFromClient);

	router.post('/admin/set/activeInactive', auth.requireLogin, ctrl.setActiveInactiveFromAdmin);
  router.get('/admin/userAction/:date', auth.requireLogin, ctrl.findUserActionAdmin);
	router.put('/admin', auth.requireLogin, ctrl.updateFromAdmin);
	router.put('/client', auth.requireLogin, ctrl.updateFromClient);
	router.put('/verify/cuiEmail', auth.requireLogin, ctrl.verifyCuiEmail);
  router.get('/admin/sync/cnp', auth.requireLogin, ctrl.syncCnp);
	router.post('/resetPwd', auth.requireLogin, ctrl.resetPwd);
	router.post('/verifyPwd', auth.requireLogin, ctrl.verifyPwd);
	router.post('/resetPwdSelf', auth.requireLogin, ctrl.resetPwdSelf);


	router.get('/activity/:id', auth.requireLogin, ctrl.activity);
	router.get('/history/:id_user/:clauseType/:limit', auth.requireLogin, ctrl.history);

	router.delete('/admin/:id_unit', ctrl.destroyFromAdmin);
	router.delete('/client/:id', ctrl.destroyFromUser);
	router.post('/mailContact/send', ctrl.sendMailContact);
	router.get('/client/hand/book/files', ctrl.getHandbookFiles);
	return router;
};