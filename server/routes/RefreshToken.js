module.exports = (app) => {
    'use strict';

    let express = require('express'),
        ctrl = require('../controllers/refreshTokenCtrl')(app.locals.db),
        router = express.Router();

    router.get('/', ctrl.refreshToken);

    return router;
};