module.exports = (app) => {
	'use strict';

	let express = require('express'),
		ctrl = require('../../controllers/drafts/countyCtrl')(app.locals.db),
		router = express.Router();

	router.get('/', ctrl.findAll);

	return router;
};