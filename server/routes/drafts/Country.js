module.exports = (app) => {
    'use strict';

    let express = require('express'),
        ctrl = require('../../controllers/drafts/countryCtrl')(app.locals.db),
        router = express.Router();

    router.get('/', ctrl.findAll);

    return router;
};