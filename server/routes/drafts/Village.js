module.exports = (app) => {
	'use strict';

	let express = require('express'),
		ctrl = require('../../controllers/drafts/villageCtrl')(app.locals.db),
		router = express.Router();

	router.get('/all', ctrl.findAll);
	router.get('/byLocality/:id_locality', ctrl.findByLocality);
	router.get('/findLocalitiesVillages/:id_county/:id_locality/:type', ctrl.findLocalitiesVillages);
	router.get('/findFull/:id_county/:id_locality/:type', ctrl.findFull);

	return router;
};