module.exports = (app) => {
	'use strict';

	let express = require('express'),
		ctrl = require('../../controllers/drafts/localityCtrl')(app.locals.db),
		router = express.Router();

	router.get('/byCounty/:id_county', ctrl.findByCounty);
	router.get('/byCountyVillage/:id_county/:village', ctrl.findByCountyVillage);

	return router;
};