module.exports = app => {
  'use strict';

  let express = require('express'),
    ctrl = require('../controllers/personCtrl')(app.locals.db),
    auth = require('../utils/authentication'),
    router = express.Router();

  router.get('/:id', auth.requireLogin, ctrl.find);
  router.post('/', auth.requireLogin, ctrl.create);
  router.get('/', auth.requireLogin, ctrl.findAll);
  router.put('/', auth.requireLogin, ctrl.update);
  router.delete('/:id', auth.requireLogin, ctrl.destroy);

  return router;
};