(function(){
    'use strict';
    module.exports = function(sequelize, DataType){
        let Address = require('./address')(sequelize, DataType),
            Unit = sequelize.define('Unit',{
                name: {
                    type: DataType.STRING
                },
                cui: {
                    type: DataType.INTEGER,
                    allowNull: false,
                    unique: true
                },
                email: {
                    type: DataType.STRING,
                    unique: true
                },
                phone: {
                    type: DataType.STRING
                },
                emblem: {
                    type: DataType.TEXT
                },
                header: {
                    type: DataType.TEXT
                },
                cap2a_local: {
                    type: DataType.BOOLEAN
                },
                cap2a_more: {
                    type: DataType.BOOLEAN
                },
                cap15a: {
                    type: DataType.BOOLEAN
                },
                cap15b: {
                    type: DataType.BOOLEAN
                },
                save_final: {
                    type: DataType.BOOLEAN
                },
                form: {
                    type: DataType.JSON
                },
                website: {
                    type: DataType.STRING
                },
                make_history: {
                    type: DataType.BOOLEAN
                },
                calc_apia: {
                    type: DataType.BOOLEAN
                },
                ran_acces: {
                    type: DataType.BOOLEAN
                },
                cap15: {
                    type: DataType.BOOLEAN
                }
            },{
                timestamps: true
            });
        Unit.belongsTo(Address, {foreignKey: 'id_address'});
        return Unit;
    };
})();
