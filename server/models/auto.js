(function () {
  'use strict';
  module.exports = function (sequelize, DataType) {
    let Person = require('./person')(sequelize, DataType),
      Auto = sequelize.define('Auto', {
        category: {
          type: DataType.STRING
        },
        body: {
          type: DataType.STRING
        },
        mark: {
          type: DataType.STRING
        },
        type: {
          type: DataType.STRING
        },
        approval_number: {
          type: DataType.STRING
        },
        manufacturing_year: {
          type: DataType.STRING
        },
        identification_number: {
          type: DataType.STRING
        },
        motor_series: {
          type: DataType.STRING
        },
        cylinder: {
          type: DataType.STRING
        },
        energy_source: {
          type: DataType.STRING
        },
        color: {
          type: DataType.STRING
        },
        first_registration: {
          type: DataType.DATE
        },
        registration_number: {
          type: DataType.STRING
        },
        book_identification: {
          type: DataType.STRING
        },
        id_buyer: {
          type: DataType.INTEGER
        }
      }, {
        timestamps: true
      });
    Auto.belongsTo(Person, {foreignKey: 'id_person'});
    return Auto;
  };
})();
