(function () {
  'use strict';
  module.exports = function (sequelize, DataType) {
    let Locality = require('./locality')(sequelize, DataType),
      Village = sequelize.define('Village', {
        name: {
          type: DataType.STRING
        },
        postal_code: {
          type: DataType.STRING
        },
        siruta: {
          type: DataType.STRING
        }
      }, {
        timestamps: true
      });
    Village.belongsTo(Locality, {foreignKey: 'id_locality', onDelete: 'cascade'});
    return Village;
  };
})();
