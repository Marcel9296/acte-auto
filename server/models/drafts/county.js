(function () {
  'use strict';
  module.exports = function (sequelize, DataType) {
    let County = sequelize.define('County', {
      name: {
        type: DataType.STRING
      },
      region: {
        type: DataType.INTEGER
      },
      siruta: {
        type: DataType.STRING
      }
    }, {
      timestamps: true
    });
    return County;
  };
})();
