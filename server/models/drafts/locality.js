(function () {
  'use strict';
  module.exports = function (sequelize, DataType) {
    let County = require('./county')(sequelize, DataType),
      Locality = sequelize.define('Locality', {
        type: {
          type: DataType.INTEGER
        },
        name: {
          type: DataType.STRING
        },
        county_code: {
          type: DataType.INTEGER
        },
        siruta: {
          type: DataType.STRING
        }
      }, {
        timestamps: true
      });
    Locality.belongsTo(County, {foreignKey: 'id_county', onDelete: 'cascade'});
    return Locality;
  };
})();
