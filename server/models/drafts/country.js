(function () {
  'use strict';
  module.exports = function (sequelize, DataTypes) {
    let Country = sequelize.define('Country', {
      name: {
        type: DataTypes.STRING
      },
      code: {
        type: DataTypes.INTEGER
      }
    });
    return Country;
  };
})();