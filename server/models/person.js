(function () {
    'use strict';
    module.exports = function (sequelize, DataType) {
        let Address = require('./address')(sequelize, DataType),
            Unit = require('./unit')(sequelize, DataType),
            Person = sequelize.define('Person', {
                pf: {
                    type: DataType.BOOLEAN
                },
                first_name: {
                    type: DataType.STRING
                },
                last_name: {
                    type: DataType.STRING
                },
                cnp: {
                    type: DataType.STRING
                },
                phone: {
                    type: DataType.STRING
                },
                email: {
                    type: DataType.STRING
                },
                mentions: {
                    type: DataType.TEXT
                },
                exit_date: {
                    type: DataType.DATE
                },
                dead: {
                    type: DataType.BOOLEAN
                },
                series: {
                    type: DataType.STRING
                },
                number: {
                    type: DataType.STRING
                },
                release_date: {
                    type: DataType.DATE
                },
                expiration_date: {
                    type: DataType.DATE
                },
                type: {
                    type: DataType.STRING
                },
                released: {
                    type: DataType.STRING
                },
                initial_father: {
                    type: DataType.STRING
                },
                invalid_cnp: {
                    type: DataType.BOOLEAN
                },
                id_person: {
                    type: DataType.INTEGER
                }
            }, {
                timestamps: true
            });
        Person.belongsTo(Address, {foreignKey: 'id_address', onDelete: 'cascade'});
        Person.belongsTo(Unit, {foreignKey: 'id_unit', onDelete: 'cascade'});
        return Person;
    };
})();
