(function () {
  'use strict';
  module.exports = function (sequelize, DataType) {
    let County = require('./drafts/county')(sequelize, DataType),
      Locality = require('./drafts/locality')(sequelize, DataType),
      Village = require('./drafts/village')(sequelize, DataType),
      Country = require('./drafts/country')(sequelize, DataType),
      Address = sequelize.define('Address', {
        street: {
          type: DataType.STRING
        },
        number: {
          type: DataType.STRING
        },
        block: {
          type: DataType.STRING
        },
        staircase: {
          type: DataType.STRING
        },
        floor: {
          type: DataType.STRING
        },
        apartment: {
          type: DataType.STRING
        },
        external_address: {
          type: DataType.STRING
        }
      }, {
        timestamps: true
      });
    Address.belongsTo(County, {foreignKey: 'id_county'});
    Address.belongsTo(Locality, {foreignKey: 'id_locality'});
    Address.belongsTo(Village, {foreignKey: 'id_village'});
    Address.belongsTo(Country, {foreignKey: 'id_country'});
    return Address;
  };
})();
