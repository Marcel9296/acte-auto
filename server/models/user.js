(function () {
    'use strict';
    let auth = require('../utils/authentication');
    module.exports = function (sequelize, DataTypes) {
        let Unit = require('./unit')(sequelize, DataTypes),
            User = sequelize.define('User', {
                    first_name: {
                        type: DataTypes.STRING
                    },
                    last_name: {
                        type: DataTypes.STRING
                    },
                    email: {
                        type: DataTypes.STRING,
                        unique: true,
                        allowNull: false
                    },
                    role: {
                        type: DataTypes.STRING
                    },
                    phone: {
                        type: DataTypes.STRING
                    },
                    owner: {
                        type: DataTypes.INTEGER
                    },
                    active: {
                        type: DataTypes.BOOLEAN
                    },
                    activity: {
                        type: DataTypes.JSON
                    },
                    password: {
                        type: DataTypes.STRING
                    },
                    //hashed_pwd: {
                    //    type: DataTypes.STRING,
                    //    allowNull: false
                    //},
                    salt: {
                        type: DataTypes.STRING,
                        allowNull: false,
                        defaultValue: auth.createSalt
                    },
                    last_login: {
                        type: DataTypes.DATE,
                        defaultValue: sequelize.NOW
                    },
                    current_farm: {
                        type: DataTypes.JSON
                    },
                    locality_user: {
                        type: DataTypes.ARRAY(DataTypes.INTEGER)
                    },
                    current_year: {
                        type: DataTypes.INTEGER
                    },
                    farm_type_user: {
                        type: DataTypes.ARRAY(DataTypes.INTEGER)
                    },
                    show_notify: {
                        type: DataTypes.BOOLEAN
                    },
                    condition: {
                        type: DataTypes.BOOLEAN
                    },
                    condition_date: {
                        type: DataTypes.DATEONLY
                    }
                },
                {
                    instanceMethods: {
                        authenticate: function (password) {
                            return auth.hashPwd(this.salt, password) === this.hashed_pwd;
                        },
                        toJSON: function () {
                            let values = this.get({plain: true});
                            delete values.password;
                            delete values.salt;
                            return values;
                        }
                    },
                    timestamps: true
                });

        User.addHook('beforeCreate', function beforeCreateUser(user, options, fn) {
            user.password = auth.hashPwd(user.salt, user.password);
            if (fn) {
                fn(null, user);
            } else {
                return user;
            }
        });

        User.belongsTo(Unit, {foreignKey: 'id_unit', onDelete: 'cascade'});
        return User;
    };
}());