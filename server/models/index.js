'use strict';
module.exports = function getModels(sequelize){
    let fs      = require('fs'),
        path      = require('path'),
        lodash    = require('lodash'),
        db = {};

    var fileTree = [];
    function getFilesRecursive (folder) {
        var fileContents = fs.readdirSync(folder), stats;
        fileContents.forEach(function (fileName) {
            stats = fs.lstatSync(folder + '/' + fileName);
            if (stats.isDirectory()) {
                getFilesRecursive(folder + '/' + fileName);
            } else {
                if(((fileName.indexOf('.') !== 0) && (fileName !== 'index.js') && (fileName.slice(-3) === '.js'))) {
                    fileTree.push(folder + '/' + fileName);
                }
            }
        });
        return fileTree;
    }

    getFilesRecursive(__dirname).forEach(function(file) {
        var model = sequelize.import(path.join(file));
        db[model.name] = model;
    });

    return lodash.extend({ sequelize: sequelize }, db);
};