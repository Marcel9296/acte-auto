(function () {
    'use strict';

    let errors = require('./errors'),
        auth = require('./utils/authentication');

    module.exports = (app) => {

        app.post('/', require('./utils/jwtInit')(app));
        app.use('/api/user', auth.requireLogin, require('./routes/User')(app));
        app.use('/api/refreshToken', require('./routes/RefreshToken')(app));
        app.use('/auth', require('./routes/Authentication'));
        app.use('/api/person', auth.requireLogin, require('./routes/Person')(app));
        app.use('/api/auto', auth.requireLogin, require('./routes/Auto')(app));

        app.use('/api/country', auth.requireLogin, require('./routes/drafts/Country')(app));
        app.use('/api/county', auth.requireLogin, require('./routes/drafts/County')(app));
        app.use('/api/locality', auth.requireLogin, require('./routes/drafts/Locality')(app));
        app.use('/api/village', auth.requireLogin, require('./routes/drafts/Village')(app));


        app.get('/app/*', (req, res) => {
            res.render('../../public/app/' + req.params['0']);
        });

        app.route('*/:url(api|auth|components|app|bower_components|assets)/*').get(errors[404]);
        app.route('/').get((req, res) => {
            res.render('login');
        });
        app.route('*')
            .get(errors[404]);
    };
}());