module.exports = (db) => {
  'use strict';
  const jwt = require('jsonwebtoken'),
    jwtRefresh = require('jsonwebtoken-refresh'),
    saveError = require("../utils/utils")(db).saveError;

  return {
    refreshToken: (req, res) => {
      let token = req.headers['x-access-token'] || req.body.token || req.params.token,
        response = null;
      if (token) {
        jwt.verify(token, global.config.sKey, function checkToken(err) {
          if (!err) {
            let obj = jwt.decode(token);
            db.sequelize.query('Select active from "User" where id =' + obj.id).then((resp) => {
              if (resp[0].length && resp[0][0].active === true) {
                let origDec = jwt.decode(token, {complete: true});
                response = jwtRefresh.refresh(origDec, 60 * 60, global.config.sKey, null);
              }
              res.send({token: response});
            }).catch(err => saveError(req.user, 'Find user for refresh token', err, res));
          } else {
            res.send({token: response});
          }
        });
      } else {
        res.send({token: response});
      }
    }
  };
};