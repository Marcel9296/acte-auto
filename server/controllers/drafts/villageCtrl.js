module.exports = db => {
  'use strict';
  const saveError = require("../../utils/utils")(db).saveError;
  const async = require('async');

  return {
    findAll: (req, res) => {
      db.sequelize.query('select v.id, v.name as village, l.name as locality, c.name as county from "Village" v ' +
      'left join "Locality" l on l.id = v.id_locality ' +
      'left join "County" c on c.id = l.id_county').then(resp => {
        res.json(resp[0]);
      }).catch(err => saveError(req.user, 'find All Villages', err, res));
    },

    findByLocality: (req, res) => {
      db.sequelize.query('select id, name, postal_code, siruta from "Village" where id_locality = ' + req.params.id_locality).then(resp => {
        res.json(resp[0]);
      }).catch(err =>  saveError(req.user, "find Street and Villages", err, res));
    },

    findLocalitiesVillages: (req, res) => {
      let response = {}, asyncTasks = [];
      asyncTasks.push((callback) => {
        db.sequelize.query('select id, name from "Locality" where id_county = ' + req.params.id_county + ' and type = ' + req.params.type + ' order by name').then(resp => {
          response.localities = resp[0];
          callback();
        }).catch(err => callback(err));
      });

      asyncTasks.push(callback => {
        db.sequelize.query('select id, name from "Village" where id_locality = ' + req.params.id_locality + ' order by name').then(resp => {
          response.villages = resp[0];
          callback();
        }).catch(err => callback(err));
      });

      async.parallel(asyncTasks, err => {
        if (err) {
          saveError(req.user, "find findLocalitiesVillages", err, res);
        } else {
          res.json(response);
        }
      });
    },

    findFull: (req, res) => {
      let response = {}, asyncTasks = [];

      asyncTasks.push(callback => {
        db.sequelize.query('select id, name from "County" order by name').then(resp => {
          response.counties = resp[0];
          callback();
        }).catch(err => callback(err));
      });

      asyncTasks.push(callback => {
        db.sequelize.query('select id, name from "Locality" where id_county = ' + req.params.id_county + ' and type = ' + req.params.type + ' order by name').then(resp => {
          response.localities = resp[0];
          callback();
        }).catch(err => callback(err));
      });

      asyncTasks.push(callback => {
        db.sequelize.query('select id, name from "Village" where id_locality = ' + req.params.id_locality + ' order by name').then(resp => {
          response.villages = resp[0];
          callback();
        }).catch(err => callback(err));
      });

      async.parallel(asyncTasks, err => {
        if (err) {
          saveError(req.user, "find findFull", err, res);
        } else {
          res.json(response);
        }
      });
    }
  };
};