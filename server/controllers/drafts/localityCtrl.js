module.exports = db => {
  'use strict';
  const saveError = require("../../utils/utils")(db).saveError;
  const replaceDiacritice = require("../../utils/utils")(db).replaceDiacritice;
  const async = require("async");

  return {
    findByCounty: (req, res) => {
      db.sequelize.query('select id, type, name from "Locality" where id_county = ' + req.params.id_county + ' order by name').then(resp => {
        res.json(resp[0]);
      }).catch(err => saveError(req.user, "find Locality for County", err, res));
    },

    findByCountyVillage: (req, res) => {
      let response = {}, asyncTasks = [], village = replaceDiacritice(req.params.village);
      db.sequelize.query('select l.id as id_locality, l.type, v.id as id_village from "Locality" l, "Village" v where l.id_county = ' + req.params.id_county +
      ' and l.id = v.id_locality and (v.name = \'' + req.params.village + '\' or v.name = \'' + village + '\')').then(loc => {
        if (loc[0].length) {
          response = loc[0][0];
          asyncTasks.push(callback => {
            db.sequelize.query('SELECT id, type, name, county_code from "Locality" where id_county = ' + req.params.id_county + ' and type = ' + loc[0][0].type + ' order by name').then(resp => {
              response.localities = resp[0];
              callback();
            }).catch(err => callback(err));
          });

          asyncTasks.push(callback => {
            db.sequelize.query('SELECT id, name, postal_code FROM "Village" where id_locality = ' + response.id_locality + ' order by name;').then(resp => {
              response.villages = resp[0];
              callback();
            }).catch(err => callback(err));
          });
        }

        async.parallel(asyncTasks, err => {
          if (err) {
            saveError(req.user, "find LocalityByCountyVillage", err, res);
          } else {
            res.json(response);
          }
        });
      }).catch(err => saveError(req.user, "find localiate", err, res));
    }
  };
};