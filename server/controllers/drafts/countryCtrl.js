module.exports = db => {
  'use strict';
  const saveError = require('../../utils/utils')(db).saveError;

  return {
    findAll: (req, res) => {
      db.sequelize.query('select id, name from "Country"').then(resp => {
        res.json(resp[0]);
      }).catch(err => saveError(req.user, 'find all Country', err, res));
    }
  };
};