module.exports = db => {
  'use strict';
  const {success: rhs, error: rh} = require('../utils/requestHandler'),
    saveError = require("../utils/utils")(db).saveError,
    async = require("async"),
    _ = require("lodash"),
    logAction = require("../utils/utils")(db).logAction;

  return {
    create: (req, res) => {
      db.Auto.create(req.body).then(resp => {
        if (resp.id) {
          rhs(res);
        } else {
          saveError(req.user, 'Create Auto', 'no such Auto', res);
        }
      }).catch(err => saveError(req.user, 'create  auto', err, res))
    },

    find: (req, res) => {
      db.sequelize.query('select * from "Auto" where id = ' + req.params.id).then(resp => {
        res.json(resp[0][0])
      }).catch(err => saveError(req.user, 'find  auto', err, res))
    },

    findAll: (req, res) => {
      db.sequelize.query('select * from "Auto" where id_person = ' + req.params.id_person).then(resp => {
        res.json(resp[0])
      }).catch(err => saveError(req.user, 'find  auto', err, res))
    },

    update: (req, res) => {
      db.Auto.update(req.body, {where: {id: req.body.id}}).then(resp => {
        if (resp[0] > 0) {
          rhs(res);
        } else {
          saveError(req.user, 'Update Auto', 'no such Auto', res);
        }
      }).catch(err => saveError(req.user, 'Update Auto', err, res));
    },

    destroy: (req, res) => {
      db.Auto.destroy({where: {id: req.params.id}}).then(resp => {
        if (resp) {
          rhs(res);
        } else {
          rhs(res, 'Destroy Person', 'no such Person');
        }
      }).catch(err => saveError(req.user, "destroy Person", err, res));
    }

  }
};