module.exports = (db) => {
  'use strict';
  const {success: rhs , error: rh} = require('../utils/requestHandler'),
    auth = require('../utils/authentication'),
    randomString = require('../utils/utils')(db).randomString,
    // emailSender = require('../utils/emailSender')(),
    async = require('async'),
    _ = require("lodash"),
    saveError = require("../utils/utils")(db).saveError,
    getFilesRecursive = require('../utils/utils')(db).getFilesRecursive;

  return {
    createFromAdmin: (req, res) => {
      req.body.password = !!req.body.password ? req.body.password : randomString(6);
      if (req.body.role === global.config.roles.admin || req.body.role === global.config.roles.superAdmin) {
        db.User.create(req.body).then(() => {
          // emailSender.sentMail('cont de administrator', req.body.email, req.body.password);
          rhs(res);
        }).catch(err => saveError(req.user, "create User from admin - role admin", err, res));
      } else {
        db.Address.create(req.body.unit.address).then(add => {
          if (add.id) {
            req.body.unit.id_address = add.id;
            db.Unit.create(req.body.unit).then(unit => {
              if (unit.id) {
                req.body.id_unit = unit.id;
                let asyncTasks = [];
                asyncTasks.push(callback => {
                  db.User.create(req.body).then(user => {
                    callback();
                  }).catch(err => callback(err));
                });
                async.parallel(asyncTasks, err => {
                  if (err) {
                    saveError(req.user, "create User from admin", err, res);
                  } else {
                    // emailSender.sentMail(req.body.unit.name, req.body.email, req.body.password);
                    rhs(res);
                  }
                });
              } else {
                rh(res, 'Create Unit', 'no such Unit');
              }
            }).catch(err => saveError(req.user, "Create Unit", err, res));
          } else {
            rh(res, 'Create Address Unit', 'no such Address');
          }
        }).catch(err => saveError(req.user, "Create Address Unit", err, res));
      }
    },

    createFromClient: (req, res) => {
      db.User.create(req.body).then(resp => {
        for (let i = 0, ln = req.body.userRight.length; i < ln; i++) {
          req.body.userRight[i].id_user = resp.id;
          req.body.userRight[i].year = new Date().getFullYear();
        }
        db.UserRight.bulkCreate(req.body.userRight).then(() => {
          // emailSender.sentMail(req.user.unit.name, req.body.email, req.body.password);
          res.json(resp.id ? resp : {});
        }).catch(err => saveError(req.user, "create User from client - user right", err, res));
      }).catch(err => saveError(req.user, "create User from client", err, res));
    },

    findFromAdmin: (req, res) => {
      db.sequelize.query('select us.id, us.first_name, us.last_name, us.email, us.phone, us.locality_user, us.farm_type_user, u.id as idu, u.name, u.cui, u.cap2a_local, u.cap2a_more, u.cap15a, ' +
        'u.cap15b, u.save_final, u.make_history, u.calc_apia, u.cap15, us.role, a.id as ida, a.street, a.number, a.id_county, a.id_locality, a.id_village, c.name as county, l.name as locality, ' +
        'l.type, v.name as village from "User" us ' +
      'left join "Unit" u on us.id_unit = u.id ' +
      'left join "Address" a on a.id = u.id_address ' +
      'left join "County" c on c.id = a.id_county ' +
      'left join "Locality" l on l.id = a.id_locality ' +
      'left join "Village" v on v.id = a.id_village ' +
      'where us.id = ' + req.params.id + ' group by us.id, u.id, a.id, c.id, l.id, v.id').then(resp => {
        res.json(resp[0].length ? {
          id: resp[0][0].id,
          first_name: resp[0][0].first_name,
          last_name: resp[0][0].last_name,
          email: resp[0][0].email,
          phone: resp[0][0].phone,
          role: resp[0][0].role,
          locality_user: resp[0][0].locality_user,
          farm_type_user: resp[0][0].farm_type_user,
          unit: {
            id: resp[0][0].idu,
            name: resp[0][0].name,
            cui: resp[0][0].cui,
            cap2a_local: resp[0][0].cap2a_local,
            cap2a_more: resp[0][0].cap2a_more,
            cap15a: resp[0][0].cap15a,
            cap15b: resp[0][0].cap15b,
            save_final: resp[0][0].save_final,
            make_history: resp[0][0].make_history,
            calc_apia: resp[0][0].calc_apia,
            cap15: resp[0][0].cap15,
            address: {
              id: resp[0][0].ida,
              street: resp[0][0].street,
              number: resp[0][0].number,
              id_county: resp[0][0].id_county,
              id_locality: resp[0][0].id_locality,
              id_village: resp[0][0].id_village,
              county: resp[0][0].county,
              locality: resp[0][0].locality,
              type: resp[0][0].type,
              village: resp[0][0].village
            }
          }
        } : {});
      }).catch(err => saveError(req.user, "find User from admin", err, res));
    },

    findFromClient: (req, res) => {
      db.sequelize.query('SELECT u.id, u.first_name, u.last_name, u.email, u.phone, u.locality_user, u.farm_type_user, u.show_notify, to_json(array_remove(array_agg(r), null)) as "userRight" FROM "User" u ' +
      'LEFT JOIN "UserRight" r ON r.id_user = u.id and r.year = ' + req.params.year + ' ' +
      'WHERE u.id = ' + req.params.id + ' group by u.id').then(resp => {
        res.json(resp[0].length ? resp[0][0] : {});
      }).catch(err => saveError(req.user, "find User from client", err, res));
    },

    findAllFromAdmin: (req, res) => {
      db.sequelize.query('select us.id, us.first_name, us.last_name, us.email, us.last_login, us."createdAt", us.active, us.id_unit, us.role, us.phone, us.condition, us.condition_date, ' +
      'u.name, u.cui, c.name as county, l.name as locality, v.name as village from "User" us ' +
      'left join "Unit" u on us.id_unit = u.id ' +
      'left join "Address" a on a.id = u.id_address ' +
      'left join "County" c on c.id = a.id_county ' +
      'left join "Locality" l on l.id = a.id_locality ' +
      'left join "Village" v on v.id = a.id_village ' +
      'where us.role in (\'' + config.roles.clientAdmin + '\',\'' + config.roles.client + '\',\'' + config.roles.admin + '\') order by us."createdAt"').then(resp => {
        res.json(resp[0]);
      }).catch(err => saveError(req.user, "find all User from admin", err, res));
    },

    findAllFromClient: (req, res) => {
      db.sequelize.query('select id, first_name, last_name, email, last_login, phone, role, password, locality_user, farm_type_user, show_notify from "User" ' +
      'where id_unit = ' + req.user.id_unit + ' and role = \'' + config.roles.client + '\' order by "createdAt"').then(resp => {
        res.json(resp[0]);
      }).catch(err => saveError(req.user, "find all User from client", err, res));
    },

    findUserActionAdmin: (req, res) => {
      db.sequelize.query('SELECT un.name, un.cui, ua.action, ua.date, ua.details ' +
      'from "UserAction" ua ' +
      'left join "User" u on u.id = ua.id_user ' +
      'left join "Unit" un on un.id = u.id_unit ' +
      'WHERE to_char(ua.date, \'YYYY-MM-DD\') = \'' + req.params.date + '\' ORDER BY ua.date desc').then(resp => {
        res.json(resp[0]);
      }).catch(err => saveError(req.user, "find User Action from admin", err, res));
    },

    setActiveInactiveFromAdmin: (req, res) => {
      db.User.update(req.body, {where: {id_unit: req.body.id_unit}}).then(() => rhs(res)).catch(err => saveError(req.user, 'set activeInactive from admin', err, res));
    },

    updateFromAdmin: (req, res) => {
      if (req.body.role === global.config.roles.admin) {
        db.User.update(req.body, {where: {id: req.body.id}}).then(() => {
          rhs(res);
        }).catch(err => saveError(req.user, "update User from admin - role admin", err, res));
      } else {
        let asyncTasks = [];
        if (req.body.unit && req.body.unit.address) {
          asyncTasks.push(callback => {
            db.Address.update(req.body.unit.address, {where: {id: req.body.unit.address.id}}).then(() => {
              callback();
            }).catch(err => callback(err));
          });
        }
        if (req.body.unit) {
          asyncTasks.push(callback => {
            db.Unit.update(req.body.unit, {where: {id: req.body.unit.id}}).then(() => {
              callback();
            }).catch(err => callback(err));
          });
        }
        asyncTasks.push(callback => {
          db.User.update(req.body, {where: {id: req.body.id}}).then(() => {
            callback();
          }).catch(err => callback(err));
        });
        async.parallel(asyncTasks, err => {
          if (err) {
            saveError(req.user, "Update User from admin", err, res);
          } else {
            rhs(res);
          }
        });
      }
    },

    updateFromClient: (req, res) => {
      let tasks = [],
        toCreate = _.filter(req.body.userRight, r => {
          r.id_user = req.body.id;
          return !r.id;
        }),
        toUpdate = _.filter(req.body.userRight, r => r.id);


      tasks.push(cb => {
        db.User.update(req.body, {where: {id: req.body.id}}).then(() => {
          cb();
        }).catch(err => cb(err));
      });

      if (toCreate.length) {
        tasks.push(cb => {
          db.UserRight.bulkCreate(toCreate).then(() => {
            cb();
          }).catch(err => cb(err));
        });
      }

      if (toUpdate.length) {
        _.each(toUpdate, r => {
          tasks.push(cb => {
            db.UserRight.update(r, {where: {id: r.id}}).then(() => {
              cb();
            }).catch(err => cb(err));
          });
        });
      }

      async.parallel(tasks, err => {
        if (err) {
          saveError(req.user, "update User from client", err, res);
        } else {
          rhs(res);
        }
      });
    },

    verifyCuiEmail: (req, res) => {
      let asyncTasks = [],
        response = {isEmail: false, isCui: false};

      if (req.body.email) {
        asyncTasks.push(callback => {
          db.sequelize.query('select id from "User" where email = \'' + req.body.email + '\'').then(resp => {
            response.isEmail = resp[0].length > 0;
            callback();
          }).catch(err => callback(err));
        });
      }

      if (req.body.cui) {
        asyncTasks.push(callback => {
          db.sequelize.query('select id from "Unit" where cui = ' + req.body.cui).then(resp => {
            response.isCui = resp[0].length > 0;
            callback();
          }).catch(err => callback(err));
        });
      }

      async.parallel(asyncTasks, err => {
        if (err) {
          saveError(req.user, "Verify Cui/Email", err, res);
        } else {
          res.json(response);
        }
      });
    },

    resetPwd: (req, res) => {
      db.sequelize.query(`Select id, salt from "User" where id = + ${req.body.id}`).then(resp => {
        if (resp[0].length) {
          let pass = !!req.body.pass ? req.body.pass : randomString(6),
            user = resp[0][0];
          db.User.update({password: auth.hashPwd(user.salt, pass)}, {where: {id: user.id}}).then(() => {
            // emailSender.sentMailResetPWD(req.body.name, req.body.email, pass);
            rhs(res);
          });
        } else {
          rh(res, 'No user found', null);
        }
      }).catch(err => saveError(null, "get user for reset pwd", err, res));
    },

    resetPwdSelf: (req, res) => {
      db.sequelize.query(`Select salt from "User" where id = + ${req.user.id}`).then(resp => {
        if (resp[0].length) {
          let hashed_pwd = auth.hashPwd(resp[0][0].salt, req.body.pass);
          db.User.update({password: hashed_pwd}, {where: {id: req.user.id}}).then(() => {
            emailSender.sentMailResetPWD(req.user.unit.name, req.user.email, req.body.pass);
            rhs(res);
          });
        } else {
          rh(res, 'No user found', null);
        }
      }).catch(err => saveError(null, "get user for reset pwd", err, res));
    },

    verifyPwd: (req, res) => {
      db.sequelize.query('Select salt from "User" where id = ' + req.user.id).then(resp => {
        if (resp[0].length) {
          let hashed_pwd = auth.hashPwd(resp[0][0].salt, req.body.oldPass);
          db.sequelize.query('Select salt from "User" where password = \'' + hashed_pwd + '\' and id = ' + req.user.id).then(resp => {
            res.json({isPass: resp[0].length > 0});
          }).catch(err => saveError(null, "get user for verify old pwd", err, res));
        } else {
          rh(res, 'No user found', null);
        }
      }).catch(err => saveError(null, "get user for verify pwd", err, res));
    },

    activity: (req, res) => {
      db.sequelize.query('select activity from "User" where id = ' + req.params.id).then(resp => {
        if (resp[0].length && resp[0][0].activity) {
          res.json(resp[0][0].activity.log);
        } else {
          res.json([]);
        }
      }).catch(err => saveError(req.user, "get User activity", err, res));
    },

    history: (req, res) => {
      db.sequelize.query('select * from "UserAction" where id_user = ' + req.params.id_user + ' order by id DESC ' + req.params.clauseType + ' ' + req.params.limit).then(resp => {
        res.json(resp[0]);
      }).catch(err => saveError(req.user, 'get User activity', err, res));
    },

    destroyFromAdmin: (req, res) => {
      db.Unit.destroy({where: {id: req.params.id_unit}}).then(resp => {
        if (resp) {
          rhs(res);
        } else {
          rh(res, 'Destroy User', 'no such User');
        }
      }).catch(err => saveError(req.user, "destroy User", err, res));
    },

    destroyFromUser: (req, res) => {
      db.User.destroy({where: {id: req.params.id}}).then(resp => {
        if (resp) {
          rhs(res);
        } else {
          rh(res, 'Destroy User', 'no such User');
        }
      }).catch(err => saveError(req.user, "destroy User", err, res));
    },

    sendMailContact: (req, res) => {
      emailSender.sentMailContact(req.body, res);
    },

    getHandbookFiles: (req, res) => {
      let handbook = [];
      getFilesRecursive(global.config.path + '/public/app/client/handbook/', 'pdf', handbook);
      res.json(handbook);
    },

    syncCnp: (req, res) => {
      db.sequelize.query('select p.id, p.cnp, p.first_name, p.last_name ' +
      'from "Person" p ' +
      'where p.invalid_cnp is null').then(resp =>{
        let ids = [], ln = resp[0].length;
        let tasks = [], response = {cnp_gresite: 0, cnp_corecte: 0};
        for (let i = 0; i < ln; i++) {
          if(resp[0][i].id !== null) {
            if (wrongCnp(resp[0][i].cnp)) {
              ids.push(resp[0][i].id);
            }
          }
        }

        if (ids.length > 0) {
          tasks.push(cb => {
            db.sequelize.query('UPDATE "Person" SET invalid_cnp = true WHERE id IN (' + ids + ')').then(resp => {
              response.cnp_gresite = resp[1].rowCount;
              cb();
            }).catch(err => cb(err));
          });
        }
        if (ids.length && ln - ids.length) {
          tasks.push(cb => {
            db.sequelize.query('UPDATE "Person" SET invalid_cnp = false WHERE id NOT IN (' + ids + ')').then(resp => {
              response.cnp_corecte = resp[1].rowCount;
              cb();
            }).catch(err => cb(err));
          });
        }
        async.parallel(tasks, err => {
          if (!err) {
            res.json(response);
          } else {
            saveError(req.user, 'Sync CNP - parallel', err, res);
          }
        });
      }).catch(err => {
        saveError(req.user, 'Sync CNP', err, res);
      });
    }
  };

  function wrongCnp(cnp) {
    if (cnp && cnp.length === 13) {
      let months = [], counties = [],
        control = [2, 7, 9, 1, 4, 6, 3, 5, 8, 2, 7, 9],
        days = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
      for (let i = 1; i < 47; i++) {
        if (i < 10) {
          counties.push('0' + i);
          months.push('0' + i);
        } else {
          counties.push('' + i);
          if (i < 13) {
            months.push('' + i);
          }
        }
      }
      counties.push('51');
      counties.push('52');
      let c = [];
      for (let i = 0, ln = cnp.length; i < ln; i++) {
        c.push(parseInt(cnp[i]));
      }
      if (c[0] < 1 || c[0] > 8) {
        return true;
      }
      let ind = months.indexOf(cnp.substring(3, 5));
      if (ind === -1) {
        return true;
      } else {
        let d = days[ind];
        if (parseInt(cnp.substring(1, 3)) % 4 === 0 && ind === 1) {
          d = 29;
        }
        if (parseInt(cnp.substring(5, 7)) > d) {
          return true;
        }
      }
      //if (counties.indexOf(cnp.substring(7, 9)) === -1) {
      //  //toastr.error('Codul județului invalid');
      //  return true;
      //}
      let sum = 0;
      for (let i = 0, ln = control.length; i < ln; i++) {
        sum += c[i] * control[i];
      }
      let controlNumber = (sum % 11) === 10 ? 1 : sum % 11;
      if (controlNumber === c[12]) {
        //toastr.success('CNP valid');
        return false;
      } else {
        //toastr.error('CNP invalid');
        return true;
      }
    } else {
      return false;
    }
  }
};