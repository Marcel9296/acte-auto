module.exports = db => {
  'use strict';
  const {success: rhs, error: rh} = require('../utils/requestHandler'),
    saveError = require("../utils/utils")(db).saveError,
    async = require("async"),
    _ = require("lodash"),
    logAction = require("../utils/utils")(db).logAction;

  return {
    create: (req, res) => {
      req.body.id_unit = req.user.id_unit;
      db.Address.create(req.body.address).then(adr => {
        req.body.id_address = adr.id;
        db.Person.create(req.body).then(pers => {
          if(req.body.buyer){
            db.sequelize.query(`UPDATE "Auto" set id_buyer = ${pers.id} where id = ${req.body.idAuto}`).catch(err=> {saveError(req.user, 'update auto from create Person', err, res)});
          }
          res.json(pers)
        }).catch(err => {
          saveError(req.user, 'create person', err, res);
        });
      }).catch(err => {
        saveError(req.user, 'create person address', err, res);
      });
    },

    update: (req, res) => {
      let t = [];
      t.push(cb => {
        db.Person.update(req.body, {where: {id: req.body.id}}).then(() => {
          cb();
        }).catch(err => cb(err));
      });

      t.push(cb => {
        if (req.body.address.id) {
          db.Address.update(req.body.address, {where: {id: req.body.address.id}}).then(() => {
            cb();
          }).catch(err => {
            cb(err);
          });
        } else {
          db.Address.create(req.body.address).then(resp => {
            req.body.address.id = resp.id;
            req.body.id_address = resp.id;
            cb();
          }).catch(err => {
            cb(err);
          });
        }
      });

      async.parallel(t, err => {
        if (err) {
          saveError(req.user, 'update Person', err, res);
        } else {
          rhs(res);
        }
      })
    },

    find: (req, res) => {
      db.sequelize.query('SELECT  p.*, to_json(adr) as address ' +
        'FROM "Person" p ' +
        'left join "Address" adr on adr.id = p.id_address ' +
        'left join "Country" c on c.id = adr.id_country ' +
        'left join "County" cou on cou.id = adr.id_county ' +
        'left join "Locality" l on l.id = adr.id_locality ' +
        'where p.id = ' + req.params.id).then(resp => {
        res.json(resp[0][0]);
      }).catch(err => saveError(req.user, "find person", err, res));
    },

    findAll: (req, res) => {
      db.sequelize.query('SELECT  p.id, concat(p.first_name, \' \', p.last_name) as name, p.cnp, cou.name as county, l.name as locality ' +
        'FROM "Person" p ' +
        'left join "Address" adr on adr.id = p.id_address ' +
        'left join "County" cou on cou.id = adr.id_county ' +
        'left join "Locality" l on l.id = adr.id_locality ' +
        'where p.id_unit = ' + req.user.id_unit + ' and p.id_person is null').then(resp => {
        res.json(resp[0]);
      }).catch(err => saveError(req.user, "find all persoane", err, res));
    },

    destroy: (req, res) => {
      db.Person.destroy({where: {id: req.params.id}}).then(resp => {
        if (resp) {
          rhs(res);
        } else {
          rh(res, 'Destroy Person', 'no such Person');
        }
      }).catch(err => saveError(req.user, "destroy Person", err, res));
    },
  }
};