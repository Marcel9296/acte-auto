'use strict';

module.exports = function initPg(config) {
  return new Promise(function initPgCb(resolve, reject) {
    const Sequelize = require('sequelize');
    const Op = Sequelize.Op;
    const operatorsAliases = {
      $eq: Op.eq,
      $ne: Op.ne,
      $gte: Op.gte,
      $gt: Op.gt,
      $lte: Op.lte,
      $lt: Op.lt,
      $not: Op.not,
      $in: Op.in,
      $notIn: Op.notIn,
      $like: Op.like,
      $notLike: Op.notLike,
      $contains: Op.contains,
      $and: Op.and,
      $or: Op.or,
      $any: Op.any,
      $all: Op.all
    };

    const sequelize = new Sequelize(config.dbUrl, {
      operatorsAliases: operatorsAliases,
      dialect: 'postgres',
      protocol: 'postgres',
      dialectOptions: {
        ssl: config.dbSsl ? config.dbSsl : false
      },
      logging: config.dbLogging,
      define: {
        timestamps: false,
        freezeTableName: true
      }
    });

    const db = require(".././models")(sequelize);
    sequelize
      .sync()
      .finally(function afterDbSync(err) {
        if (err) {
          console.log('Init pg connexion', err);
          reject(err);
        } else {
          // const dbScripts = require("./dbScripts");
          // dbScripts.addDefaultUser(db);
          // dbScripts.syncCnpPerson(db);
          resolve(db);
        }
      });
  });
};
