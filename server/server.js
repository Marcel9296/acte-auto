'use strict';
let cluster = require('cluster');
const worker = process.env.WEB_CONCURRENCY || 1;

if (cluster.isMaster) {
    console.log(`Master ${process.pid} is running`);
    // Fork workers.
    for (var i = 0; i < worker; i++) {
        cluster.fork();
    }

    Object.keys(cluster.workers).forEach(function (id) {
        console.log("I am running with ID : " + cluster.workers[id].process.pid);
    });

    cluster.on('exit', function (worker, code, signal) {
        console.log('worker ' + worker.process.pid + ' died');
        if (signal) {
            console.log(`worker was killed by signal: ${signal}`);
        } else if (code !== 0) {
            console.log(`worker exited with error code: ${code}`);
        } else {
            console.log('worker success!');
        }
        cluster.fork();
    });
} else {

    global.NODE_ENV = process.env.NODE_ENV = process.env.NODE_ENV || 'dev';
    const express = require('express');
    const config = require('./init/config').init();
    global.config = config;

    let app = express();
    let server = require('http').createServer(app);
    let io = require('socket.io')(server, {
        'transports': ['websocket', 'polling'],
        pingInterval: 15000,
        pingTimeout: 30000
    });
    let redis = require('socket.io-redis');
    let pg = require('./db/initPg');
    let phantomInit = require('./init/phantomInit');
    // let emailTransport = require('./init/emailTransport');
    io.adapter(redis(process.env.REDIS_URL));

    Promise.all([pg(config), phantomInit.createPhantomSession(app)]).then(values => {
        const [db, phSession] = values;
        app.locals.config = config;
        // app.locals.email = global.smtpTransportYour = emailTransport.createTransport();
        app.locals.db = db;
        app.locals.ph = phSession;
        //let dbScripts =  require('./db/dbScripts');
        //dbScripts.syncUserRight(db);
        //dbScripts.addDefaultUser(db);

        require('./init/express')(app, config);
        require('./routes')(app);

        io.on('connection', socket => {
            socket.on('join', data => {
                socket.join(data.id);
                //console.log('io.sockets rooms', io.sockets.adapter.rooms);
            });
            socket.on('accountDisabled', user => io.sockets.in(user.id).emit('accountDisabled', user));
            socket.on('getActiveUsers', () => {
                io.of('/').adapter.allRooms((err, rooms) => {
                    let idArr = [];
                    for (let i = 0; i < rooms.length; i++) {
                        if (!isNaN(rooms[i])) {
                            idArr.push(parseInt(rooms[i]));
                        }
                    }
                    if (idArr.length) {
                        db.sequelize.query('SELECT us.id, us.surname, us.forename, us.email, us."createdAt", us.active, us.id_unit, us.role, us.phone, us.last_login, ' +
                            'u.name, u.cui, case when unit_type = 1 then \'Învăţământ\' when unit_type = 2 then \'UAT\' end AS unit_type, ' +
                            'c.name AS county, l.name AS locality, v.name AS village, us.condition, us.condition_date ' +
                            'FROM "User" us ' +
                            'LEFT JOIN "Unit" u ON us.id_unit = u.id ' +
                            'LEFT JOIN "Address" a ON a.id_unit = u.id ' +
                            'LEFT JOIN "County" c ON c.id = a.id_county ' +
                            'LEFT JOIN "Locality" l ON l.id = a.id_locality ' +
                            'LEFT JOIN "Village" v ON v.id = a.id_village ' +
                            'WHERE (us.role = \'clientAdmin\' OR us.role = \'client\') AND  us.id IN (' + idArr + ') ' +
                            'ORDER BY us."createdAt"').then(resp => {
                            io.sockets.emit('getActiveUsers', resp[0]);
                        }).catch(err => console.log('err get active users ', err));
                    } else {
                        io.sockets.emit('getActiveUsers', []);
                    }
                });
            });
            socket.on('draftCorChanged', () => io.sockets.emit('draftCorChanged'));
            socket.on('draftContractChanged', () => io.sockets.emit('draftContractChanged'));
            socket.on('keywordChanged', () => io.sockets.emit('keywordChanged'));
            socket.on('accountDisabled', user => io.sockets.emit('accountDisabled', user));
            socket.on('pageReload', users => io.sockets.emit('pageReload', users));
        });
        io.of('/').adapter.on('error', err => console.log('ERROR no redis server', err));

        server.listen(config.port, config.ip, () => {
            console.log('Listening on port: %d, env: %s', config.port, config.env);
            process.on('exit', () => {
                console.log('exiting phantom session');
                app.locals.ph.exit();
            });
        });
    }).catch(reason => {
        console.log('Init sequence error:', reason);
    });

    module.exports = app;
}