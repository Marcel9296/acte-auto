(function() {

    'use strict';

    module.exports = function (app, config) {
        let express = require('express'),
            morgan = require('morgan'),
            compression = require('compression'),
            bodyParser = require('body-parser'),
            cookieParser = require('cookie-parser'),
            favicon = require('serve-favicon'),
            path = require('path'),
            env = config.env,
            helmet = require('helmet');

        app.use(compression());
        app.use(helmet());
        app.disable('x-powered-by');
        app.use(express.static(path.join(config.path, '/public')));
        app.set('views', config.path + '/server/views');
        app.set('view engine', 'pug');
        app.use(favicon('./public/app/images/barn.ico'));

        app.use(bodyParser.json({limit: '10mb'}));
        app.use(bodyParser.urlencoded({limit: '10mb', extended: false}));
        app.use(cookieParser());

        if ('production' === env || 'staging' === env) {
            app.set('appPath', config.path + '/public');
            app.use(morgan('dev'));
        }

        if ('dev' === env || 'test' === env) {
            app.use(require('connect-livereload')());
            app.disable('view cache');
            app.set('appPath', config.path + '/public');
            app.use(morgan('dev'));
        }
    };
}());
