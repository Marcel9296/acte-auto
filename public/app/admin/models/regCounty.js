(function () {
  'use strict';
  angular.module('acte-auto').factory('regCounty', $resource => {
    return {
      simple: $resource('/api/county')
    };
  });
}());