(function () {
  'use strict';
  angular.module('acte-auto').factory('regYears', $resource => {
    return {
      simple: $resource('/api/years/:id')
    };
  });
})();