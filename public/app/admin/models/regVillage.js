(function () {
  'use strict';
  angular.module('acte-auto').factory('regVillage', $resource => {
    return {
      byLocality: $resource('/api/village/byLocality/:id_locality', {id_locality: '@id_locality'}),
      findFull: $resource('/api/village/findFull/:id_county/:id_locality/:type', {id_county: '@id_county', id_locality: '@id_locality', type: '@type'})
    };
  });
}());