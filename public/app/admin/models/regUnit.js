(function () {
  'use strict';
  angular.module('acte-auto').factory('regUnit', $resource => {
    return {
      simple: $resource('/api/unit', {}, {
        'update': {method: 'PUT'}
      }),
      forTransfer: $resource('/api/unit/admin/forTransfer'),
      dbInfo: $resource('/api/unit/dbInfo/:id', {id: '@id'})
    };
  });
}());