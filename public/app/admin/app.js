(function () {
  'use strict';

  angular.module('acte-auto', [
    'ngResource',
    'ngRoute',
    'ngStorage',
    'ui.grid',
    'ui.grid.selection',
    'ui.grid.resizeColumns',
    'ui.grid.exporter',
    'ui.bootstrap',
    'dialogs.main',
    'toastr',
    'ui.select',
    'ngSanitize',
    'infinite-scroll'
  ]);

  function RequestService($q, $location, $localStorage) {
    return {
      request: function (config) {
        config.headers = config.headers || {};
        if ($localStorage.token) {
          config.headers['x-access-token'] = $localStorage.token;
        }
        return config;
      },

      responseError: function (response) {
        if (response.status === 401 || response.status === 403) {
          $location.path('/');
        }
        return $q.reject(response);
      }
    };
  }

  function config($routeProvider, $httpProvider, $locationProvider, $translateProvider, $uibTooltipProvider) {
    $httpProvider.interceptors.push('RequestService');
    $locationProvider.hashPrefix('');
    $translateProvider.translations('ro', {
      DIALOGS_OK: 'Ok',
      DIALOGS_YES: "Da",
      DIALOGS_NO: "Nu"
    });
    $uibTooltipProvider.options({appendToBody: true, placement: 'left'});
    $routeProvider
      .when('/', {
        templateUrl: 'app/admin/main/dash',
        controller: 'dashCtrl'
      })
      .when('/users', {
        templateUrl: 'app/admin/user/users',
        controller: 'usersCtrl'
      })
      .when('/logError', {
        templateUrl: 'app/admin/logError/logError',
        controller: 'logErrorCtrl'
      })
      .when('/logAction', {
        templateUrl: 'app/admin/logAction/logAction',
        controller: 'logActionCtrl'
      })
      .when('/ranUnit', {
        templateUrl: 'app/admin/ranUnit/ranUnit',
        controller: 'ranUnitCtrl'
      })
      .when('/userRight', {
        templateUrl: 'app/admin/userRight/userRight',
        controller: 'userRightCtrl'
      })

      // -------------------------------- Impozite si taxe ----------------------
      .when('/logErrorTaxes', {
        templateUrl: 'app/admin/taxes/logErrorTaxes/logErrorTaxes',
        controller: 'logErrorTaxesCtrl'
      })
      .when('/logActionTaxes', {
        templateUrl: 'app/admin/taxes/logActionTaxes/logActionTaxes',
        controller: 'logActionTaxesCtrl'
      })
      // -------------------------------- DRAFTS --------------------------------
      .when('/chapterDraft', {
        templateUrl: 'app/admin/drafts/chapterDraft/chapterDraft',
        controller: 'chapterDraftCtrl'
      })
      .when('/definitionDraft', {
        templateUrl: 'app/admin/drafts/definitionDraft/definitionDraft',
        controller: 'definitionDraftCtrl'
      })
      .when('/exploitationTypeDraft', {
        templateUrl: 'app/admin/drafts/exploitationTypeDraft/exploitationTypeDraft',
        controller: 'exploitationTypeDraftCtrl'
      })
      .when('/farmTypeDraft', {
        templateUrl: 'app/admin/drafts/farmTypeDraft/farmTypeDraft',
        controller: 'farmTypeDraftCtrl'
      })
      // -------------------------------- END DRAFTS --------------------------------
      .when('/recalculation', {
        templateUrl: 'app/admin/recalculation/recalculation',
        controller: 'recalculationCtrl'
      })
      .when('/transfer', {
        templateUrl: 'app/admin/transfer/transfer',
        controller: 'transferCtrl'
      })
      .when('/updates', {
        templateUrl: 'app/admin/updates/updates',
        controller: 'updatesCtrl'
      })
      .when('/condition', {
        templateUrl: 'app/admin/condition/condition',
        controller: 'conditionCtrl'
      })
      .when('/queryDatabase', {
        templateUrl: 'app/admin/queryDatabase/queryDatabase',
        controller: 'queryDatabaseCtrl'
      })
      .when('/farmCorrection', {
        templateUrl: 'app/admin/farmCorrection/farmCorrection',
        controller: 'farmCorrectionCtrl'
      })
      .when('/modifyLocality', {
        templateUrl: 'app/admin/modifyLocality/modifyLocality',
        controller: 'modifyLocalityCtrl'
      })

      .otherwise({redirectTo: '/'});
  }

  function run($rootScope, $location, $route) {
    $rootScope.$on('$routeChangeError', function (evt, currentUser, previous, rejection) {
      if (rejection === 'not authorized') {
        $location.path('/');
      }
      $route.reload();
    });
  }

  function socketFactory() {
    //let socket = io.connect({'transports': ['websocket', 'polling'], forceNew: true});
    let socket = io.connect({'transports': ['websocket'], forceNew: true});
    return {
      on: (eventName, callback) => {
        socket.on(eventName, callback);
      },
      emit: (eventName, data) => {
        socket.emit(eventName, data);
      },
      off: eventName => {
        if (eventName) {
          socket.off(eventName);
        } else {
          socket.removeAllListeners();
        }
      }
    };
  }

  angular.module('acte-auto').filter('propsFilter', function () {
    return function (items, props) {
      var out = [];
      if (angular.isArray(items)) {
        items.forEach(function (item) {
          var itemMatches = false;

          var keys = Object.keys(props);
          for (var i = 0; i < keys.length; i++) {
            var prop = keys[i];
            var text = props[prop].toLowerCase();
            if (item[prop].toString().toLowerCase().indexOf(text) !== -1) {
              itemMatches = true;
              break;
            }
          }

          if (itemMatches) {
            out.push(item);
          }
        });
      } else {
        // Let the output be the input untouched
        out = items;
      }

      return out;
    };
  });

  config
    .$inject = ['$routeProvider', '$httpProvider', '$locationProvider', '$translateProvider', '$uibTooltipProvider'];

  run
    .$inject = ['$rootScope', '$location', '$route'];

  RequestService
    .$inject = ['$q', '$location', '$localStorage'];

  angular
    .module('acte-auto')
    .factory('RequestService', RequestService)
    .factory('socket', socketFactory)
    .config(config)
    .run(run);
}());


angular.module('ui.grid').config(['$provide', function ($provide) {
  'use strict';
  $provide.decorator('i18nService', ['$delegate', function ($delegate) {
    $delegate.add('en', {
      headerCell: {
        aria: {
          defaultFilterLabel: 'Filter for column',
          removeFilter: 'Remove Filter',
          columnMenuButtonLabel: 'Column Menu'
        },
        priority: 'Priority:',
        filterLabel: "Filter for column: "
      },
      aggregate: {
        label: 'items'
      },
      groupPanel: {
        description: 'Drag a column header here and drop it to group by that column.'
      },
      search: {
        placeholder: 'Cautare...',
        showingItems: 'Showing :',
        selectedItems: ' Înregistrări selectate:',
        totalItems: 'Total înregistrări:',
        size: 'Page Size:',
        first: 'First Page',
        next: 'Next Page',
        previous: 'Previous Page',
        last: 'Last Page'
      },
      menu: {
        text: 'Choose Columns:'
      },
      sort: {
        ascending: 'Sortare crescător',
        descending: 'Sortare descrescător',
        none: 'Sort None',
        remove: 'Anulează sortarea'
      },
      column: {
        hide: 'Ascunde coloana'
      },
      aggregation: {
        count: 'total rows: ',
        sum: 'total: ',
        avg: 'avg: ',
        min: 'min: ',
        max: 'max: '
      },
      pinning: {
        pinLeft: 'Pin Left',
        pinRight: 'Pin Right',
        unpin: 'Unpin'
      },
      columnMenu: {
        close: 'Close'
      },
      gridMenu: {
        aria: {
          buttonLabel: 'Grid Menu'
        },
        columns: 'Coloane:',
        importerTitle: 'Import file',
        exporterAllAsCsv: 'Export all data as csv',
        exporterVisibleAsCsv: 'Export data în csv',
        exporterSelectedAsCsv: 'Export selected data as csv',
        exporterAllAsPdf: 'Export all data as pdf',
        exporterVisibleAsPdf: 'Export data în pdf',
        exporterSelectedAsPdf: 'Export selected data as pdf',
        clearAllFilters: 'Şterge filtre'
      },
      importer: {
        noHeaders: 'Column names were unable to be derived, does the file have a header?',
        noObjects: 'Objects were not able to be derived, was there data in the file other than headers?',
        invalidCsv: 'File was unable to be processed, is it valid CSV?',
        invalidJson: 'File was unable to be processed, is it valid Json?',
        jsonNotArray: 'Imported json file must contain an array, aborting.'
      },
      pagination: {
        aria: {
          pageToFirst: 'Page to first',
          pageBack: 'Page back',
          pageSelected: 'Selected page',
          pageForward: 'Page forward',
          pageToLast: 'Page to last'
        },
        sizes: 'items per page',
        totalItems: 'items',
        through: 'through',
        of: 'of'
      },
      grouping: {
        group: 'Group',
        ungroup: 'Ungroup',
        aggregate_count: 'Agg: Count',
        aggregate_sum: 'Agg: Sum',
        aggregate_max: 'Agg: Max',
        aggregate_min: 'Agg: Min',
        aggregate_avg: 'Agg: Avg',
        aggregate_remove: 'Agg: Remove'
      },
      validate: {
        error: 'Error:',
        minLength: 'Value should be at least THRESHOLD characters long.',
        maxLength: 'Value should be at most THRESHOLD characters long.',
        required: 'A value is needed.'
      }
    });
    return $delegate;
  }]);
}]);


angular.module('acte-auto').config(function (toastrConfig) {
  'use strict';
  angular.extend(toastrConfig, {
    autoDismiss: false,
    containerId: 'toast-container',
    maxOpened: 0,
    newestOnTop: true,
    positionClass: 'toast-top-right',
    preventDuplicates: false,
    preventOpenDuplicates: true,
    target: 'body'
  });
});