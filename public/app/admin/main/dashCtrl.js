(function () {
	"use strict";
	dashCtrl.$inject = ['$location', '$scope'];
	angular.module('acte-auto').controller('dashCtrl', dashCtrl);
	function dashCtrl($location, $scope) {
    $scope.trustAsHtml = function(description) {
      return $sce.trustAsHtml(description);
    };
	}
}());
