(function() {
  'use strict';
  showPdfCtrl.$inject = ['$scope', '$uibModalInstance'];
  angular.module('acte-auto').controller('showPdfCtrl', showPdfCtrl);
  function showPdfCtrl($scope, $uibModalInstance) {
    let isCrome = navigator.userAgent.match(/Chrom(e|ium)\/([0-9]+)\./);
    let cromeVersion = isCrome ? parseInt(navigator.userAgent.match(/Chrom(e|ium)\/([0-9]+)\./)[2]) : null;
    $scope.showIfrm = cromeVersion && cromeVersion <= 50 ? true : false;

    $scope.cancel = () => {
      $uibModalInstance.dismiss();
    };
  }
}());