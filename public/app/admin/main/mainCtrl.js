(function () {
	"use strict";
	mainCtrl.$inject = ['$scope', '$rootScope', '$location', 'utils', '$localStorage', '$uibModal', '$uibModalStack', 'toastr', 'dialogs', 'regUser'];
	angular.module('acte-auto').controller('mainCtrl', mainCtrl);
	function mainCtrl($scope, $rootScope, $location, utils, $localStorage, $uibModal, $uibModalStack, toastr, dialogs, regUser) {
		$localStorage.token = window.bootstrappedUser.token;
		$scope.userName = window.bootstrappedUser.userName;
		$scope.superAdmin = window.bootstrappedUser.role === 'sa';
    // $scope.config = {uiColor: '#f2f5f7', language: 'ro', autoGrow_onStartup: true,
    //   toolbarGroups:[
    //     { name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
    //     { name: 'clipboard', groups: [ 'clipboard', 'undo' ] },
    //     { name: 'editing', groups: [ 'find', 'selection', 'spellchecker', 'editing' ] },
    //     { name: 'forms', groups: [ 'forms' ] },
    //     { name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi', 'paragraph' ] },
    //     { name: 'links', groups: [ 'links' ] },
    //     { name: 'insert', groups: [ 'insert' ] },
    //     { name: 'styles', groups: [ 'styles' ] },
    //     { name: 'colors', groups: [ 'colors' ] },
    //     { name: 'document', groups: [ 'mode', 'document', 'doctools' ] },
    //     { name: 'tools', groups: [ 'tools' ] },
    //     { name: 'others', groups: [ 'others' ] }
    //   ],
			// height: '800px', skin: 'office2013', extraPlugins: 'sourcedialog',
    //   removeButtons: 'BGColor,Anchor,Subscript,Superscript,Paste,Copy,Cut,Undo,Redo,Print,Source,NewPage'
    // };

		$rootScope.$on('$locationChangeStart', () => {
			$uibModalStack.dismissAll('cancel');
		});

		// regLogError.count.get().$promise.then(resp => {
		// 	$scope.logs = resp.count;
		// 	if (resp.count !== 0) {
		// 		$location.path('logError');
		// 	}
		// }).catch(() => toastr.error('A apărut o eroare'));

		// $scope.refreshLogErrors = deleteAll => {
		// 	if (deleteAll) {
		// 		$scope.logs = null;
		// 	} else {
		// 		$scope.logs = $scope.logs - 1;
		// 	}
		// };

		$scope.editProfile = () => {
			$uibModal.open({
				templateUrl: 'app/admin/main/profile/profile-modal',
				controller: 'profileCtrl',
				backdrop: 'static',
				size: 'md',
				scope: $scope
			}).result.then(resp => $scope.userName = resp.userName);
		};

		$scope.resetPw = () => {
			$uibModal.open({
				templateUrl: 'app/admin/main/resetPwdSelf/resetPwdSelf-modal',
				controller: 'resetPwdSelf',
				backdrop: 'static',
				size: 'md'
			});
		};

    $scope.syncCnp = () => {
      dialogs.confirm('Confirmă sincronizarea', 'Doriţi să verificați dacă CNP-urile dn baza de date sunt greșite ?', {size: 'sm'}).result.then(() => {
        regUser.syncCnp.get().$promise.then(resp => {
          dialogs.notify('Sincronizare CNP-uri', resp.cnp_gresite + ' - CNP-uri greșite <br> ' + resp.cnp_corecte + ' - CNP-uri corecte').result.catch(() => {
          });
        }).catch(() => {
          toastr.error('Eroare la validarea CNP-urilor');
        });
      }).catch(() => {
      });
    };

		$scope.logOut = () => {
			utils.resetLS();
		};
	}
}());