(function () {
    'use strict';
    usersCtrl.$inject = ['$scope', '$uibModal', 'socket', 'dialogs', 'toastr', 'utils', 'regUser'];
    angular.module('acte-auto').controller('usersCtrl', usersCtrl);

    function usersCtrl($scope, $uibModal, socket, dialogs, toastr, utils, regUser) {
        let allUsers;
        console.log('aci')
        regUser.simple.query().$promise.then(resp => {
            $scope.gridUsers.data = resp;
            allUsers = _.cloneDeep(resp);
            utils.setHeight('grUser', resp.length);
            $(window).trigger('resize');
        }).catch(() => {
            toastr.error('A apărut o eroare');
        });

        $(window).resize(() => utils.setHeight('grUser', $scope.gridUsers.data.length));
        $scope.$on('$destroy', () => $(window).unbind('resize'));

        $scope.allUsers = () => {
            $scope.gridUsers.data = allUsers;
            utils.setHeight('grUser', allUsers.length);
            $(window).trigger('resize');
            $scope.userStatus = '';
        };

        $scope.superAdmin = window.bootstrappedUser.role === 'sa';
        let active = '<div class="m-top-4 text-center"><a ng-if="row.entity.active === true" class="btn btn-success btn-xs width100" ng-click="grid.appScope.setActiveInactive(row.entity)" uib-tooltip="Dezactivează cont">Activ</a>' +
            '<a ng-if="row.entity.active === false || row.entity.active === null" class="btn btn-danger btn-xs width100" ng-click="grid.appScope.setActiveInactive(row.entity)" uib-tooltip="Activează cont">Inactiv</a></div>',
            activity = '<div class="m-top-4 text-center"><a class="btn btn-info btn-xs" ng-click="grid.appScope.getActivity(row.entity)" uib-tooltip="Activitate utilizator"><i class="glyphicon glyphicon-barcode"></i></a></div>',
            history = '<div class="m-top-4 text-center"><a class="btn btn-default btn-xs margin-top4" ng-click="grid.appScope.getHistory(row.entity)" uib-tooltip="Istoric actiuni utilizator"><i class="glyphicon glyphicon-compressed"></i></a></div>',
            resetPwd = '<div class="m-top-4 text-center"><a class="btn btn-warning btn-xs" ng-click="grid.appScope.resetPwd(row.entity)" uib-tooltip="Resetre parolă"><i class="glyphicon glyphicon-lock"></i></a></div>',
            dbInfo = '<div class="m-top-4 text-center"><a ng-if="row.entity.role !== \'admin\'" class="btn btn-default btn-xs" ng-click="grid.appScope.getDbInfo(row.entity)" uib-tooltip="Info DB"><i class="glyphicon glyphicon-hdd"></i></a></div>',
            destroy = '<div class="m-top-4 text-center" ><a ng-if="grid.appScope.superAdmin" class="btn btn-danger btn-xs" ng-click="grid.appScope.destroy(row.entity)" uib-tooltip="Ştergere cont"><i class="glyphicon glyphicon-trash"></i></a></div>',
            edit = '<div class="m-top-4 text-center"><a class="btn btn-primary btn-xs " ng-click="grid.appScope.edit(row.entity)" uib-tooltip="Editare cont"><i class="glyphicon glyphicon-edit"></i></a></div>';
        $scope.gridUsers = {
            enableFiltering: true,
            enableRowSelection: true,
            enableRowHeaderSelection: false,
            multiSelect: false,
            enableGridMenu: true,
            showGridFooter: true,
            //exporterOlderExcelCompatibility: true,
            exporterCsvFilename: 'users.csv',
            exporterMenuPdf: false,
            exporterMenuExcel: false,
            //exporterPdfTableStyle: {fontSize: 8,margin: [10, 10, 10, 10]},
            //exporterPdfTableHeaderStyle: {fontSize: 10, bold: true, italics: true},
            //exporterPdfOrientation: 'landscape',
            //exporterPdfMaxGridWidth: 650,
            exporterMenuAllData: false,
            exporterCsvLinkElement: angular.element(document.querySelectorAll(".custom-csv-link-location")),
            columnDefs: [
                {field: 'last_name', displayName: 'Numele', filter: {condition: utils.roFilter}, minWidth: 130, enableHiding: false},
                {field: 'first_name', displayName: 'Prenumele', filter: {condition: utils.roFilter}, minWidth: 130, enableHiding: false},
                {field: 'name', displayName: 'Unitate', filter: {condition: utils.roFilter}, minWidth: 200, enableHiding: false},
                {field: 'cui', displayName: 'Cui', width: 100, enableHiding: false},
                {field: 'email', displayName: 'Email', minWidth: 170, enableHiding: true},
                {field: 'county', displayName: 'Judeţ', filter: {condition: utils.roFilter}, width: 100, enableHiding: true},
                {field: 'locality', displayName: 'Localitate', filter: {condition: utils.roFilter}, width: 110, enableHiding: true},
                {field: 'role', displayName: 'Rol', width: 90, enableHiding: true, visible: false},
                {field: 'last_login', displayName: 'Last login', width: 140, enableHiding: true, cellFilter: "date:\'dd.MM.yyyy - HH:mm\'", filterCellFiltered: true, cellClass: 'text-center'},
                {field: 'createdAt', displayName: 'Creat', width: 90, visible: false, cellFilter: "date:\'dd.MM.yyyy\'", filterCellFiltered: true, cellClass: 'text-center'},
                {field: 'phone', displayName: 'Telefon', width: 100, enableHiding: true, visible: false, cellClass: 'text-center'},
                {field: 'active', displayName: 'Activ', width: 60, enableFiltering: false, cellTemplate: active, enableHiding: true},
                {field: 'edit', displayName: '', width: 30, enableFiltering: false, cellTemplate: edit, enableHiding: true},
                {field: 'activity', displayName: '', width: 30, enableFiltering: false, cellTemplate: activity, enableHiding: true, visible: true},
                {field: 'history', displayName: '', width: 30, enableFiltering: false, cellTemplate: history, enableHiding: true, visible: true},
                {field: 'dbInfo', displayName: '', width: 30, enableFiltering: false, cellTemplate: dbInfo, enableHiding: true, visible: true},
                {field: 'resetPwd', displayName: '', width: 30, enableFiltering: false, enableHiding: true, cellTemplate: resetPwd},
                {field: 'destroy', displayName: '', width: 30, enableFiltering: false, cellTemplate: destroy, enableHiding: true}
            ],
            rowTemplate: "<div ng-dblclick='grid.appScope.edit(row.entity)' ng-repeat='(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name' class='ui-grid-cell' ng-class=\"{ 'ui-grid-row-header-cell': col.isRowHeader }\" ui-grid-cell></div>"
        };

        $scope.addUser = () => {
            $uibModal.open({
                templateUrl: 'app/admin/user/user-modal',
                controller: 'addUserCtrl',
                size: 'md'
            }).result.catch(() => {});
        };

        $scope.edit = row => {
            if (row.role === 'admin' && !$scope.superAdmin) {
                toastr.warning('Not enough privileges');
            } else {
                $scope.idUser = row.id;
                $uibModal.open({
                    templateUrl: 'app/admin/user/user-modal',
                    controller: 'editUserCtrl',
                    size: 'md',
                    scope: $scope,
                    closed: {
                        resolve: () => {
                            return $scope.items;
                        }
                    }
                }).result.catch(() => {});
            }
        };

        $scope.activeUsersNow = () => {
            socket.emit('getActiveUsers');
            socket.on('getActiveUsers', resp => {
                socket.off('getActiveUsers');
                if (!!resp.length) {
                    $scope.gridUsers.data = resp;
                    utils.setHeight('grUser', resp.length);
                    $(window).trigger('resize');
                    $scope.userStatus = 'online';
                } else {
                    toastr.info('Nu există utilizatori online în acest moment');
                }
            });
        };

        $scope.activeUsersToday = () => {
            let d = (new Date()).setHours(0, 0, 0, 0);
            let tmp = _.filter(allUsers, f => new Date(f.last_login).setHours(0, 0, 0, 0) === d);
            if (!!tmp.length) {
                $scope.gridUsers.data = tmp;
                utils.setHeight('grUser', tmp.length);
                $(window).trigger('resize');
                $scope.userStatus = 'autentificați astăzi';
            } else {
                toastr.info('Nu există utilizatori autentificaţi astăzi');
            }
        };

        $scope.getActivity = row => {
            $scope.row = row;
            $uibModal.open({
                templateUrl: 'app/admin/user/activity/activity-modal',
                controller: 'activityCtrl',
                size: 'sm',
                scope: $scope
            }).result.catch(() => {});
        };

        $scope.getHistory = row => {
            $scope.row = row;
            $uibModal.open({
                templateUrl: 'app/admin/user/history/history-modal',
                controller: 'historyCtrl',
                size: 'lg',
                scope: $scope
            }).result.catch(() => {});
        };

        $scope.getDbInfo = row => {
            $scope.row = row;
            $uibModal.open({
                templateUrl: 'app/admin/user/dbInfo/dbInfo-modal',
                controller: 'dbInfoCtrl',
                size: 'sm',
                scope: $scope
            }).result.catch(() => {});
        };

        $scope.setActiveInactive = row => {
            let active = angular.copy(row.active);
            let users = _.filter($scope.gridUsers.data, e => e.id_unit === row.id_unit && (active ? true : !e.active));
            if (row.role === 'admin' && !$scope.superAdmin) {
                toastr.warning('Not enough privileges');
            } else {
                let message = active ? 'Contul a fost dezactivat' : 'Contul a fost activat';
                if (row.role === 'clientAdmin') {
                    regUser.activeInactive.save({id_unit: row.id_unit, active: !active}).$promise.then(resp => {
                        if (resp.success) {
                            toastr.success(message);
                            for (let i = 0; i < users.length; i++) {
                                if (active) {
                                    socket.emit('accountDisabled', users[i]);
                                }
                                if (active === users[i].active) {
                                    users[i].active = !users[i].active;
                                }
                            }
                        } else {
                            toastr.error('A apărut o eroare');
                        }
                    }).catch(() => toastr.error('A apărut o eroare'));
                } else {
                    regUser.simple.update({id: row.id, active: !active}).$promise.then(resp => {
                        if (resp.success) {
                            toastr.success(message);
                            if (active) {
                                socket.emit('accountDisabled', row);
                            }
                            row.active = !active;
                        } else {
                            toastr.error('A apărut o eroare');
                        }
                    }).catch(() => toastr.error('A apărut o eroare'));
                }
            }
        };

        $scope.resetPwd = row => {
            $scope.row = row;
            $uibModal.open({
                templateUrl: 'app/admin/user/resetPwd/resetPwd-modal',
                controller: 'resetPwdCtrl',
                size: 'md',
                scope: $scope
            }).result.catch(() => {});
        };

        $scope.destroy = row => {
            if (row.role === 'clientAdmin') {
                dialogs.confirm('Confirmă ştergerea', 'Doriţi să ştergeţi contul pentru unitatea <b>' + row.name + '</b> ?', {size: 'sm'}).result.then(() => {
                    regUser.byId.remove({id: row.id_unit}).$promise.then(() => {
                        $scope.gridUsers.data.splice($scope.gridUsers.data.indexOf(row), 1);
                        toastr.success('Utilizatorul ' + row.name + ' a fost şters');
                        utils.setHeight('grUser', $scope.gridUsers.data.length);
                        $(window).trigger('resize');
                    }).catch(() => toastr.error('A apărut o eroare'));
                }, () => {});
            } else {
                dialogs.confirm('Confirmă ştergerea', 'Doriţi să ştergeţi contul pentru utilizatorul <b>' + row.last_name + ' ' + row.first_name + '</b> ?', {size: 'sm'}).result.then(() => {
                    regUser.clientById.remove({id: row.id}).$promise.then(() => {
                        $scope.gridUsers.data.splice($scope.gridUsers.data.indexOf(row), 1);
                        toastr.success('Utilizatorul ' + row.last_name + ' ' + row.first_name + ' a fost şters');
                        utils.setHeight('grUser', $scope.gridUsers.data.length);
                        $(window).trigger('resize');
                    }).catch(() => toastr.error('A apărut o eroare'));
                }, () => {});
            }
        };
    }
}());