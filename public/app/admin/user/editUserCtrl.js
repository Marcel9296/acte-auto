(function () {
  "use strict";
  editUserCtrl.$inject = ['$scope', '$q', 'toastr', '$uibModalInstance', '$location', 'regUser', 'regLocality', 'regVillage', 'regCounty', 'utils'];
  angular.module('acte-auto').controller('editUserCtrl', editUserCtrl);

  function editUserCtrl($scope, $q, toastr, $uibModalInstance, $location, regUser, regLocality, regVillage, regCounty, utils) {
    $scope.locTypes = [{name: 'Municipiu', art: 'Municipiul', val: 1, ind: 0}, {
      name: 'Oraş',
      art: 'Oraşul',
      val: 2,
      ind: 0
    }, {name: 'Comună', art: 'Comuna', val: 3, ind: 1}];
    $scope.villageTypes = [{name: 'Localitate', art: 'Localitatea', val: 1}, {name: 'Sat', art: 'Satul', val: 3}];
    let oldCui, oldEmail;

    regUser.byId.get({id: $scope.idUser}).$promise.then((resp) => {
      $scope.user = resp;
      oldCui = angular.copy(resp.unit.cui);
      oldEmail = angular.copy(resp.email);
      $scope.counties = [{id: resp.unit.address.id_county, name: resp.unit.address.county}];
      $scope.localities = [{id: resp.unit.address.id_locality, name: resp.unit.address.locality}];
      $scope.villages = [{id: resp.unit.address.id_village, name: resp.unit.address.village}];
      // if ($scope.user.userRight && $scope.user.userRight.length) {
      //   $scope.user.userRight = draft.userRight;
      // }
    }).catch(() => {
      toastr.error('A apărut o eroare');
    });

    regCounty.simple.query().$promise.then(resp => {
      $scope.county = resp;
    }).catch(() => {
      toastr.error('Eroare la preluarea județelor');
    });

    $scope.changeCounty = id => {
      $scope.user.unit.address.id_locality = null;
      $scope.localities = [];
      if (id) {
        regLocality.byCounty.query({id_county: id}).$promise.then(resp => {
          $scope.localities = resp;
        }).catch(() => toastr.error('Eroare. User-ul nu poate fi adăugat'));
      }
    };

    $scope.changeLocality = (id) => {
      $scope.user.unit.address.id_village = null;
      $scope.villages = [];
      if (id) {
        regVillage.byLocality.query({id_locality: id}).$promise.then(resp => {
          $scope.villages = resp;
        }).catch(() => toastr.error('Eroare. User-ul nu poate fi adăugat'));
      }
      $scope.user.id_village = null;
    };

    let validation = user => {
      if (user.role === 'clientAdmin') {
        if (!user.unit.cui) {
          toastr.error('Introduceţi codul fiscal al unităţii');
          return false;
        }
        if (!user.unit.name) {
          toastr.error('Introduceţi denumirea unităţii');
          return false;
        }
        if (!user.unit.address.id_county) {
          toastr.error('Selectaţi judetul');
          return false;
        }
        if (!user.unit.address.type) {
          toastr.error('Selectaţi tipul de localitate');
          return false;
        }
        if (!user.unit.address.id_locality) {
          toastr.error('Selectaţi ' + (user.unit.address.type === 1 ? 'municipiul' : (user.unit.address.type === 2 ? 'oraşul' : 'comuna')));
          return false;
        }
        if (!user.unit.address.id_village) {
          toastr.error('Selectaţi localitatea');
          return false;
        }
        if (!user.first_name) {
          toastr.error('Introduceţi numele utilizatorului');
          return false;
        }
        if (!user.last_name) {
          toastr.error('Introduceţi prenumele utilizatorului');
          return false;
        }
        if (!user.email) {
          toastr.error('Introduceţi email-ul utilizatorului');
          return false;
        }
        let EMAIL_REGEXP = /^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$/;
        if (EMAIL_REGEXP.test(user.email) === false) {
          toastr.error('Email introdus nu este corect');
          return false;
        }
        if (!user.phone) {
          toastr.error('Introduceţi un număr de telefon de contact');
          return false;
        }
        if (user.phone.length < 10) {
          toastr.error('Numărul de telefon introdus este incorect');
          return false;
        }
      } else {
        if (!user.first_name) {
          toastr.error('Introduceţi numele utilizatorului');
          return false;
        }
        if (!user.last_name) {
          toastr.error('Introduceţi prenumele utilizatorului');
          return false;
        }
        if (!user.email) {
          toastr.error('Introduceţi email-ul utilizatorului');
          return false;
        }
        let EMAIL_REGEXP = /^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$/;
        if (EMAIL_REGEXP.test(user.email) === false) {
          toastr.error('Email introdus nu este corect');
          return false;
        }
      }
      return true;
    };

    $scope.save = () => {
      $scope.veryfyCuiEmail().then(a => {
        if (validation($scope.user) && a.done) {
          regUser.simple.update($scope.user).$promise.then(resp => {
            if (resp.success) {
              toastr.success('Informaţiile au fost salvate');
            } else {
              toastr.error('Informaţiile nu au fost salvate');
            }
            $uibModalInstance.dismiss();
            $location.path('/users/');
          }).catch(() => toastr.error('A apărut o eroare'));
        }
      });
    };
  }
}());