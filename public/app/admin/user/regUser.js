(function () {
    'use strict';
    angular.module('acte-auto').factory('regUser', $resource => {
        return {
            simple: $resource('/api/user/admin', {}, {
                'update': {method: 'PUT'}
            }),
            byId: $resource('/api/user/admin/:id', {id: '@id'}),
        };
    });
}());