(function () {
  "use strict";
  addUserCtrl.$inject = ['$scope', '$q', 'toastr', '$uibModalInstance', '$location', 'utils', 'regUser', 'regCounty', 'regLocality', 'regVillage'];
  angular.module('acte-auto').controller('addUserCtrl', addUserCtrl);

  function addUserCtrl($scope, $q, toastr, $uibModalInstance, $location, utils, regUser, regCounty, regLocality, regVillage) {
    if (window.bootstrappedUser.role === 'sa') {
      $scope.roles = ['clientAdmin', 'admin', 'sa'];
    } else {
      $scope.roles = ['clientAdmin'];
    }
    $scope.user = {
      role: 'clientAdmin',
      active: true,
      unit: {cap2a_local: true, cap2a_more: true, cap15a: true, cap15b: true, make_history: false, address: {}},
      posts: [{name: 'Primar', code: 1}, {name: 'Secretar', code: 2}, {
        name: 'Registru agricol',
        code: 3
      }, {name: 'Impozite şi taxe', code: 4}],
      // userRight: draft.userRight,
      show_notify: false
    };
    $scope.locTypes = [{name: 'Municipiu', art: 'Municipiul', val: 1, ind: 0}, {
      name: 'Oraş',
      art: 'Oraşul',
      val: 2,
      ind: 0
    }, {name: 'Comună', art: 'Comuna', val: 3, ind: 1}];
    $scope.villageTypes = [{name: 'Localitate', art: 'Localitatea', val: 1}, {name: 'Sat', art: 'Satul', val: 3}];

    regCounty.simple.query().$promise.then(resp => {
      $scope.county = resp;
    }).catch(() => {
      toastr.error('Eroare la preluarea județelor');
    });

    $scope.changeCounty = id => {
      $scope.user.unit.address.id_locality = null;
      $scope.localities = [];
      if (id) {
        regLocality.byCounty.query({id_county: id}).$promise.then(resp => {
          $scope.localities = resp;
        }).catch(() => toastr.error('Eroare. User-ul nu poate fi adăugat'));
      }
    };

    $scope.changeLocality = (id) => {
      $scope.user.unit.address.id_village = null;
      $scope.villages = [];
      if (id) {
        regVillage.byLocality.query({id_locality: id}).$promise.then(resp => {
          $scope.villages = resp;
        }).catch(() => toastr.error('Eroare. User-ul nu poate fi adăugat'));
      }
      $scope.user.id_village = null;
    };

    $scope.veryfyCuiEmail = () => {
      return $q((resolve) => {
        let toVerify = {cui: $scope.user.unit.cui, email: $scope.user.email};
        if (toVerify.cui || toVerify.email) {
          regUser.verifyCuiEmail.verify(toVerify).$promise.then((resp) => {
            if (resp.isCui) {
              toastr.error('Codul fiscal introdus este utilizat');
              resolve({done: false});
            } else if (resp.isEmail) {
              toastr.error('Email-ul introdus este utilizat');
              resolve({done: false});
            } else {
              resolve({done: true});
            }
          }).catch(() => {
            toastr.error('A apărut o eroare');
          });
        } else {
          resolve({done: true});
        }
      });
    };

    let validation = (user) => {
      if (user.role === 'clientAdmin') {
        if (!user.unit.cui) {
          toastr.error('Introduceţi codul fiscal al unităţii');
          return false;
        }
        if (!user.unit.name) {
          toastr.error('Introduceţi denumirea unităţii');
          return false;
        }
        // if (!user.unit.address.id_county) {
        //   toastr.error('Selectaţi judetul');
        //   return false;
        // }
        // if (!user.unit.address.type) {
        //   toastr.error('Selectaţi tipul de localitate');
        //   return false;
        // }
        // if (!user.unit.address.id_locality) {
        //   toastr.error('Selectaţi ' + (user.unit.address.type === 1 ? 'municipiul' : (user.unit.address.type === 2 ? 'oraşul' : 'comuna')));
        //   return false;
        // }
        // if (!user.unit.address.id_village) {
        //   toastr.error('Selectaţi localitatea');
        //   return false;
        // }
        if (!user.first_name) {
          toastr.error('Introduceţi numele utilizatorului');
          return false;
        }
        if (!user.last_name) {
          toastr.error('Introduceţi prenumele utilizatorului');
          return false;
        }
        if (!user.email) {
          toastr.error('Introduceţi email-ul utilizatorului');
          return false;
        }
        let EMAIL_REGEXP = /^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$/;
        if (EMAIL_REGEXP.test(user.email) === false) {
          toastr.error('Email introdus nu este corect');
          return false;
        }
        if (!user.phone) {
          toastr.error('Introduceţi un număr de telefon de contact');
          return false;
        }
        if (user.phone.length < 10) {
          toastr.error('Numărul de telefon introdus este incorect');
          return false;
        }
      } else {
        if (!user.first_name) {
          toastr.error('Introduceţi numele utilizatorului');
          return false;
        }
        if (!user.last_name) {
          toastr.error('Introduceţi prenumele utilizatorului');
          return false;
        }
        if (!user.email) {
          toastr.error('Introduceţi email-ul utilizatorului');
          return false;
        }
        let EMAIL_REGEXP = /^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$/;
        if (EMAIL_REGEXP.test(user.email) === false) {
          toastr.error('Email introdus nu este corect');
          return false;
        }
      }
      return true;
    };

    $scope.save = () => {
      $scope.user.locality_user = _.map($scope.villages, 'id');
      $scope.user.current_year = new Date().getFullYear();
      // $scope.veryfyCuiEmail().then((a) => {
      if (validation($scope.user)) {
        delete $scope.user.unit.address.type;
        regUser.simple.save($scope.user).$promise.then(resp => {
          if (resp.success) {
            toastr.success('Utilizatorul a fost creat');
          } else {
            toastr.error('Utilizatorul nu a fost creat');
          }
          $uibModalInstance.close();
          $location.path('/users/');
        }).catch(() => {
          toastr.error('A apărut o eroare');
        });
      }
      // });
    };
  }
}());