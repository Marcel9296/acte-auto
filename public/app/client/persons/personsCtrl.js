(function () {
  "use strict";
  personsCtrl.$inject = ['$scope', '$uibModal', '$location', '$localStorage', 'toastr', 'regPerson', 'utils', 'dialogs'];
  angular.module('acte-auto').controller('personsCtrl', personsCtrl);

  function personsCtrl($scope, $uibModal, $location, $localStorage, toastr, regPerson, utils, dialogs) {
    regPerson.simple.query().$promise.then(resp => {
      $scope.gridPerson.data = resp;
      utils.setHeight('grPerson', resp.length);
      $(window).trigger('resize');
    }).catch(() => {
      toastr.error('A apărut o eroare');
    });

    let select = '<div class="text-center"><a class="btn btn-primary btn-xs m-top-4" ng-click="grid.appScope.selectPerson(row.entity)" ng-disabled="grid.appScope.disableDelet" uib-tooltip="Selectează dosarul"><i class="glyphicon glyphicon-edit"></i></a></div>',
      delet = '<div class="text-center"><button class="btn btn-danger btn-xs m-top-4" ng-disabled="grid.appScope.disableDelet" ng-click="grid.appScope.deletePerson(row.entity)" uib-tooltip="Șterge gospodăria"><i class="glyphicon glyphicon-trash"></i></button></div>';


    $scope.gridPerson = {
      enableFiltering: true,
      enableRowSelection: true,
      enableSorting: true,
      enableRowHeaderSelection: false,
      multiSelect: false,
      showGridFooter: true,
      columnDefs: [
        {
          field: 'name',
          displayName: 'Nume și prenume',
          minWidth: 60,
          cellClass: 'text-center',
          type: 'string',
          filter: {}
        },
        {
          field: 'cnp',
          displayName: 'Cnp persoană',
          minWidth: 60,
          maxWidth: 330,
          cellClass: 'text-center',
          type: 'string',
          filter: {}
        },
        {
          field: 'county',
          displayName: 'Județ',
          minWidth: 60,
          maxWidth: 330,
          cellClass: 'text-center',
          type: 'string',
          filter: {}
        },
        {
          field: 'county',
          displayName: 'Localitate',
          minWidth: 60,
          maxWidth: 330,
          cellClass: 'text-center',
          type: 'string',
          filter: {}
        },
        {field: 'select', displayName: '', width: 30, enableFiltering: false, cellTemplate: select, enableHiding: true},
        {field: 'destroy', displayName: '', width: 30, enableFiltering: false, cellTemplate: delet, enableHiding: true}
      ],
      rowTemplate: "<div ng-dblclick='grid.appScope.selectPerson(row.entity)' ng-right-click='grid.appScope.openCertificates(row.entity.id_farm)' ng-repeat='(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name' class='ui-grid-cell' ng-class=\"{\' text-red\': row.entity.status === 'ANULEAZA', \' text-info\': row.entity.status === 'DEZACTIVARE_GOSPODARIE', \' colorDead\' : row.entity.dead}\" ui-grid-cell></div>"
    };


    $(window).resize(() => {
      // utils.setHeight('grFolderss', $scope.gridFolders.data.length);
      //$scope.gridApi.core.refresh();
    });

    $scope.addPerson = () => {
      delete $localStorage.idPerson;
      $uibModal.open({
        templateUrl: 'app/client/persons/person-modal',
        controller: 'addPersonCtrl',
        size: 'lg',
        scope: $scope
      }).result.catch(() => {
      });
    };

    $scope.selectPerson = (row) => {
      $localStorage.idPerson = row.id;
      $location.path('/person');
    };

    $scope.deletePerson = (row) => {
      dialogs.confirm('Confirmă ştergerea', 'Doriţi să ştergeţi persoana <b>' + row.name + '</b> ?', {size: 'sm'}).result.then(() => {
        regPerson.byId.remove({id: row.id}).$promise.then(() => {
          $scope.gridPerson.data.splice($scope.gridPerson.data.indexOf(row), 1);
          toastr.success('Persoana ' + row.name + ' a fost şters');
          utils.setHeight('grPerson', $scope.gridPerson.data.length);
          $(window).trigger('resize');
        }).catch((err) => {
          toastr.error('A apărut o eroare')
        });
      }, () => {
      });
    }
  }
}());