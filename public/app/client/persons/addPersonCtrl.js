(function () {
  "use strict";
  addPersonCtrl.$inject = ['$scope', '$uibModalInstance', '$location', '$localStorage', 'toastr', 'regPerson', 'regCountry', 'regCounty', 'utils', 'regLocality', 'regVillage'];
  angular.module('acte-auto').controller('addPersonCtrl', addPersonCtrl);

  function addPersonCtrl($scope, $uibModalInstance, $location, $localStorage, toastr, regPerson, regCountry, regCounty, utils, regLocality, regVillage ) {
    $scope.ob = {personType: [{name: 'Persoana Fizica', val: true}, {name: 'Persoana Juridica', val: false}], identityCard:['B.I.', 'C.I.'], firmType: ['S.R.L.', 'I.I.', 'I.F.', 'A.F.', 'P.F.A', 'C.M.I.']};
    $scope.person = {address: {}};
    $scope.locTypes = [{name: 'Municipiu', art: 'Municipiul', val: 1, ind: 0}, {name: 'Oraş', art: 'Oraşul', val: 2, ind: 0 }, {name: 'Comună', art: 'Comuna', val: 3, ind: 1}];
    let t = [];

    t.push(cb => {
      regCountry.simple.query().$promise.then(resp => {
        $scope.country = resp;
        cb();
      }).catch(err => cb(err));
    });

    t.push(cb => {
      regCounty.simple.query().$promise.then(resp => {
        $scope.county = resp;
        cb();
      }).catch(err => {
        cb(err)
      });
    });

    async.parallel(t, err => {
      if(err){
        toastr.error('Eroare la preluarea datelor!');
      } else {
        if (!$localStorage.idPerson) {
          $scope.person.address.id_country = $scope.country[0].id;
          $scope.person.address.id_county = $scope.user.id_county;
          $scope.person.address.id_locality = $scope.user.id_locality;
          $scope.person.address.id_village = $scope.user.id_village;
        }

        t = [];
        t.push(cb => {
          regLocality.byCounty.query({id_county: $scope.person.address.id_county}).$promise.then(resp => {
            $scope.ob.locality = resp;
            let loc = _.find(resp, l => l.id === $scope.person.address.id_locality);
            $scope.ob.typeLocality = loc.type;
            cb();
          }).catch(err => cb(err));
        });
        t.push(cb => {
          regVillage.byLocality.query({id_locality: $scope.person.address.id_locality}).$promise.then(resp => {
            $scope.ob.village = resp;
            cb();
          }).catch(err => cb(err));
        });
        async.parallel(t, err => {
          if (err) {
            toastr.error('Probleme la preluarea datelor');
          }
          $scope.stopLoader();
        });
      }
    });

    $scope.changeCountry = utils.changeCountry;
    $scope.changeCounty = utils.changeCounty;
    $scope.changeLocality = utils.changeLocality;

    $scope.save = () => {
      regPerson.simple.save($scope.person).$promise.then(resp => {
        if (resp.id) {
          toastr.success('Persoana a fost creat');
          $localStorage.idPerson = resp.id;
          $location.path('/person');
        } else {
          toastr.error('Persoana nu a fost creat');
        }
        $uibModalInstance.close();
      }).catch(() => {
        toastr.error('A apărut o eroare');
      });
    }
  }
}());