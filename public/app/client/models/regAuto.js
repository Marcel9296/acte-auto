(function () {
    'use strict';
    angular.module('acte-auto').factory('regAuto', $resource => {
        return {
            simple: $resource('/api/auto', {}, {
                'update': {method: 'PUT'}
            }),
            byPerson: $resource('/api/auto/all/:id_person'),
            byId: $resource('/api/auto/:id')
        };
    });
})();