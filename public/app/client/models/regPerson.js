(function () {
    'use strict';
    angular.module('acte-auto').factory('regPerson', $resource => {
        return {
            simple: $resource('/api/person', {}, {
                'update': {method: 'PUT'}
            }),
            updateBuletin: $resource('/api/person/update/buletin', {}, {
                'update': {method: 'PUT'}
            }),
            byId: $resource('/api/person/:id', {id: '@id'})
        };
    });
})();