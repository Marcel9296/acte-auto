(function () {
  'use strict';
  angular.module('acte-auto').factory('regLocality', $resource => {
    return {
      byCounty: $resource('/api/locality/byCounty/:id_county'),
      byCountyVillage: $resource('/api/locality/byCountyVillage/:id_county/:village', {id_county: '@id_county', village: '@village'})
    };
  });
})();