(function () {
  'use strict';
  angular.module('acte-auto').factory('regCountry', $resource => {
    return {
      simple: $resource('/api/country')
    };
  });
})();