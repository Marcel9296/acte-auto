(function () {
  'use strict';
  angular.module('acte-auto').factory('regVillage', $resource => {
    return {
      all: $resource('/api/village/all'),
      byLocality: $resource('/api/village/byLocality/:id_locality'),
      findLocalitiesVillages: $resource('/api/village/findLocalitiesVillages/:id_county/:id_locality/:type', {id_county: '@id_county', id_locality: '@id_locality', type: '@type'}),
      findFull: $resource('/api/village/findFull/:id_county/:id_locality/:type', {id_county: '@id_county', id_locality: '@id_locality', type: '@type'})
    };
  });
})();