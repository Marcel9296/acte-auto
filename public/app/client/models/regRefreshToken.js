(function () {
  'use strict';
  angular.module('acte-auto').factory('regRefreshToken', $resource => {
    return {
      refresh: $resource('/api/refreshToken')
    };
  });
})();