(function () {
  'use strict';

  angular.module('acte-auto', [
    'ngResource',
    'ngRoute',
    'ngStorage',
    'ui.grid',
    'ui.grid.selection',
    'ui.grid.resizeColumns',
    'ui.bootstrap',
    'dialogs.main',
    'toastr',
    'ui.select',
    'ngSanitize',
    'infinite-scroll',
    'darthwade.dwLoading',
  ]);

  function RequestService($q, $location, $localStorage) {
    return {
      request: function (config) {
        config.headers = config.headers || {};
        if ($localStorage.token) {
          config.headers['x-access-token'] = $localStorage.token;
        }
        if ($localStorage.currentYear) {
          config.headers['current-year'] = $localStorage.currentYear;
        }
        return config;
      },

      responseError: function (response) {
        if (response.status === 401 || response.status === 403 || response.status === 500) {
          $localStorage.currentError = true;
          //$location.path('/contact/');
        }
        return $q.reject(response);
      }
    };
  }

  function config($routeProvider, $httpProvider, $translateProvider, $locationProvider, uiSelectConfig, $uibTooltipProvider) {
    $httpProvider.interceptors.push('RequestService');
    $locationProvider.hashPrefix('');
    uiSelectConfig.theme = 'select2';
    uiSelectConfig.appendToBody = true;
    $uibTooltipProvider.options({appendToBody: true, placement: 'left'});
    $translateProvider.translations('ro', {
      DIALOGS_YES: "Da",
      DIALOGS_NO: "Nu",
      DIALOGS_OK: "Am înțeles"
    });
    $routeProvider
      .when('/', {
        templateUrl: 'app/client/main/dash',
        controller: 'dashCtrl'
      })
      // ------------------- CONFIGURARI // -------------------
      .when('/persons', {
        templateUrl: 'app/client/persons/person',
        controller: 'personsCtrl'
      })
      .when('/person', {
        templateUrl: 'app/client/person/person',
        controller: 'personCtrl'
      })
      .when('/autos', {
        templateUrl: 'app/client/person/autos/autos',
        controller: 'autosCtrl'
      })
      .when('/registrationForm', {
        templateUrl: 'app/client/person/registrationForm/registrationForm',
        controller: 'registrationFormCtrl'
      })

      .otherwise({redirectTo: '/'});
  }

  function run($rootScope, $location, $route, $templateCache) {
    $templateCache.put("select2/match-multiple.tpl.html", "<span class=\"ui-select-match\"><li class=\"ui-select-match-item select2-search-choice\" ng-repeat=\"$item in $select.selected track by $index\" ng-class=\"{\'select2-search-choice-focus\':$selectMultiple.activeMatchIndex === $index, \'select2-locked\':$select.isLocked(this, $index)}\" ng-click=\"$selectMultiple.removeChoice($index)\" ui-select-sort=\"$select.selected\"><span uis-transclude-append=\"\"></span> <a href=\"javascript:;\" class=\"ui-select-match-close select2-search-choice-close\" tabindex=\"-1\"></a></li></span>");
    $rootScope.$on('$routeChangeError', function (evt, currentUser, previous, rejection) {
      if (rejection === 'not authorized') {
        $location.path('/');
      }
      $route.reload();
    });
  }

  function socketFactory() {
    //let socket = io.connect({'transports': ['websocket', 'polling'], forceNew: true});
    let socket = io.connect({'transports': ['websocket'], forceNew: true});
    socket.on('reconnect', () => {
      socket.emit('join', window.bootstrappedUser);
    });
    return {
      on: (eventName, callback) => {
        socket.on(eventName, callback);
      },
      emit: (eventName, data) => {
        socket.emit(eventName, data);
      },
      off: eventName => {
        if (eventName) {
          socket.off(eventName);
        } else {
          socket.removeAllListeners();
        }
      }
    };
  }

  config
    .$inject = ['$routeProvider', '$httpProvider', '$translateProvider', '$locationProvider', 'uiSelectConfig', '$uibTooltipProvider'];

  run
    .$inject = ['$rootScope', '$location', '$route', '$templateCache'];

  RequestService
    .$inject = ['$q', '$location', '$localStorage'];

  angular
    .module('acte-auto')
    .factory('RequestService', RequestService)
    .factory('socket', socketFactory)
    .config(config)
    .run(run);
}());

angular.module('acte-auto').filter('propsFilter', function () {
  'use strict';
  return function (items, props) {
    let out = [];
    if (angular.isArray(items)) {
      for (let j = 0, lng = items.length; j < lng; j++) {
        let itemMatches = false;
        let keys = Object.keys(props);
        for (let i = 0, len = keys.length; i < len; i++) {
          let prop = keys[i];
          let text = props[prop].toLowerCase();
          if (items[j][prop] && items[j][prop].toString().toLowerCase().indexOf(text) !== -1) {
            itemMatches = true;
            break;
          }
        }
        if (itemMatches) {
          out.push(items[j]);
        }
      }
    } else {
      out = items;
    }
    return out;
  };
});

function replaceDiacritice(text) {
  if(!!text){
    return text ? text.toLowerCase().normalize('NFKD').replace(/[^\w]/g, '') : null;
  }
  return text;
}

angular.module('acte-auto').filter('simpleFilter', function() {
  return function(items, props) {
    if (!!props) {
      var out = [];
      var text = replaceDiacritice(props.toString());
      for(var j = items.length-1; j >=0; j--){
        if (items[j] && replaceDiacritice(items[j].toString()).indexOf(text) !== -1) {
          out.unshift(items[j]);
        }
      }
      return out;
    } else {
      return items;
    }
  };
});

angular.module('acte-auto').filter('sumOfValue', function () {
    'use strict';
    return function(data, key) {
        //debugger;
        if (angular.isUndefined(data) || angular.isUndefined(key))
            return 0;

        let sum = 0;
        angular.forEach(data, function(v) {
            sum = Math.round((sum + parseFloat(v[key])) * 10000) / 10000;
        });
        return sum;
    };
});

angular.module('ui.grid').config(['$provide', function ($provide) {
  'use strict';
  $provide.decorator('i18nService', ['$delegate', function ($delegate) {
    $delegate.add('en', {
      headerCell: {
        aria: {
          defaultFilterLabel: 'Filter for column',
          removeFilter: 'Remove Filter',
          columnMenuButtonLabel: 'Column Menu'
        },
        priority: 'Priority:',
        filterLabel: "Filter for column: "
      },
      aggregate: {
        label: 'items'
      },
      groupPanel: {
        description: 'Drag a column header here and drop it to group by that column.'
      },
      search: {
        placeholder: 'Cautare...',
        showingItems: 'Showing :',
        selectedItems: ' Înregistrări selectate:',
        totalItems: 'Total înregistrări:',
        size: 'Page Size:',
        first: 'First Page',
        next: 'Next Page',
        previous: 'Previous Page',
        last: 'Last Page'
      },
      menu: {
        text: 'Choose Columns:'
      },
      sort: {
        ascending: 'Sort Ascending',
        descending: 'Sort Descending',
        none: 'Sort None',
        remove: 'Remove Sort'
      },
      column: {
        hide: 'Hide Column'
      },
      aggregation: {
        count: 'total rows: ',
        sum: 'total: ',
        avg: 'avg: ',
        min: 'min: ',
        max: 'max: '
      },
      pinning: {
        pinLeft: 'Pin Left',
        pinRight: 'Pin Right',
        unpin: 'Unpin'
      },
      columnMenu: {
        close: 'Close'
      },
      gridMenu: {
        aria: {
          buttonLabel: 'Grid Menu'
        },
        columns: 'Coloane:',
        importerTitle: 'Import file',
        exporterAllAsCsv: 'Export all data as csv',
        exporterVisibleAsCsv: 'Export visible data as csv',
        exporterSelectedAsCsv: 'Export selected data as csv',
        exporterAllAsPdf: 'Export all data as pdf',
        exporterVisibleAsPdf: 'Export visible data as pdf',
        exporterSelectedAsPdf: 'Export selected data as pdf',
        clearAllFilters: 'Şterge filtre'
      },
      importer: {
        noHeaders: 'Column names were unable to be derived, does the file have a header?',
        noObjects: 'Objects were not able to be derived, was there data in the file other than headers?',
        invalidCsv: 'File was unable to be processed, is it valid CSV?',
        invalidJson: 'File was unable to be processed, is it valid Json?',
        jsonNotArray: 'Imported json file must contain an array, aborting.'
      },
      pagination: {
        aria: {
          pageToFirst: 'Page to first',
          pageBack: 'Page back',
          pageSelected: 'Selected page',
          pageForward: 'Page forward',
          pageToLast: 'Page to last'
        },
        sizes: 'items per page',
        totalItems: 'items',
        through: 'through',
        of: 'of'
      },
      grouping: {
        group: 'Group',
        ungroup: 'Ungroup',
        aggregate_count: 'Agg: Count',
        aggregate_sum: 'Agg: Sum',
        aggregate_max: 'Agg: Max',
        aggregate_min: 'Agg: Min',
        aggregate_avg: 'Agg: Avg',
        aggregate_remove: 'Agg: Remove'
      },
      validate: {
        error: 'Error:',
        minLength: 'Value should be at least THRESHOLD characters long.',
        maxLength: 'Value should be at most THRESHOLD characters long.',
        required: 'A value is needed.'
      }
    });
    return $delegate;
  }]);
}]);

angular.module('acte-auto').config(function (toastrConfig) {
  'use strict';
  angular.extend(toastrConfig, {
    autoDismiss: false,
    containerId: 'toast-container',
    maxOpened: 0,
    newestOnTop: true,
    positionClass: 'toast-top-right',
    preventDuplicates: false,
    preventOpenDuplicates: true,
    target: 'body',
    progressBar: true,
    timeOut: 2000
  });
});