(function () {
  "use strict";
  conditionCtrl.$inject = ['$scope', '$uibModalInstance', 'regUser', 'toastr'];
  angular.module('acte-auto').controller('conditionCtrl', conditionCtrl);
  function conditionCtrl($scope, $uibModalInstance, regUser, toastr) {
    $scope.accept = () => {
      window.bootstrappedUser.condition = true;
      regUser.simple.update({
        id: window.bootstrappedUser.id,
        condition: true,
        condition_date: new Date()
      }).$promise.then(function (resp) {
          if (resp.success) {
            toastr.success('Condițiile au fost acceptate!');
          } else {
            toastr.error('Eroare. Condițiile nu au fost acceptate!');
          }
          $uibModalInstance.close();
        });
    };
  }
}());