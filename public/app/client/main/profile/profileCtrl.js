(function () {
  'use strict';
  profileCtrl.$inject = ['$scope', '$localStorage', '$uibModalInstance', 'toastr', 'regUser'];
  angular.module('acte-auto').controller('profileCtrl', profileCtrl);
  function profileCtrl($scope, $localStorage, $uibModalInstance, toastr, regUser) {
    regUser.byId.get({id: window.bootstrappedUser.id, year: window.bootstrappedUser.currentYear}).$promise.then(resp => {
      $scope.modal = resp;
    }).catch(() => {
      $localStorage.errorAction = 'Editare profil utilizator';
    });

    $scope.save = () => {
      if ($scope.validation($scope.modal)) {
        regUser.simple.update($scope.modal).$promise.then(resp => {
          if (resp.success) {
            toastr.success('Datele au fost salvate');
            $uibModalInstance.close({userName: $scope.modal.first_name + ' ' + $scope.modal.last_name});
          } else {
            toastr.error('Datele nu au fost salvate');
            $uibModalInstance.dismiss();
          }
        }).catch(() => {
          toastr.error('Datele nu au fost salvate');
          $uibModalInstance.dismiss();
        });
      }
    };

    $scope.validation = modal => {
      if (!modal.first_name) {
        toastr.error('Introduceţi numele');
        return false;
      }
      if (!modal.last_name) {
        toastr.error('Introduceţi penumele');
        return false;
      }
      if (!modal.phone) {
        toastr.error('Introduceţi numărul de telefon');
        return false;
      }
      return true;
    };
  }
}());