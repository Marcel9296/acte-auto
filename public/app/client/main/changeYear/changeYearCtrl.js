(function () {
  'use strict';
  changeYearCtrl.$inject = ['$scope', '$localStorage', '$uibModalInstance', 'regYears'];
  angular.module('acte-auto').controller('changeYearCtrl', changeYearCtrl);
  function changeYearCtrl($scope, $localStorage, $uibModalInstance, regYears) {
    $scope.modal = {year: $scope.currentYear};

    regYears.simple.query().$promise.then(resp => {
      $scope.years = resp;
    }).catch(() => {
      $localStorage.errorAction = 'Schimbare an de lucru';
    });

    $scope.save = () => {
      $uibModalInstance.close($scope.modal.year);
    };
  }
}());