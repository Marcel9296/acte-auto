(function () {
  'use strict';
  resetPwdSelfCtrl.$inject = ['$scope', '$q', '$uibModalInstance', 'toastr', 'regUser'];
  angular.module('acte-auto').controller('resetPwdSelfCtrl', resetPwdSelfCtrl);
  function resetPwdSelfCtrl($scope, $q, $uibModalInstance, toastr, regUser) {
    $scope.modal = {};

    $scope.checkPassword = pw => {
      return $q(resolve => {
        if (pw) {
          regUser.verifyPwd.check({oldPass: pw}).$promise.then(resp => {
            if (!resp.isPass) {
              toastr.error('Parola veche introdusă nu corespunde cu parola dumneavoastră');
              resolve({status: false});
            } else {
              resolve({status: true});
            }
          });
        } else {
          toastr.error('Introduceţi parola veche');
          resolve({status: false});
        }
      });
    };

    $scope.save = () => {
      $scope.checkPassword($scope.modal.oldPass).then(result => {
        if (result.status && $scope.validation($scope.modal)) {
          regUser.resetPwdSelf.reset({pass: $scope.modal.newPass}).$promise.then(resp => {
            if (resp.success) {
              toastr.success('Parola a fost resetată');
            } else {
              toastr.error('Parola nu a fost resetată');
            }
            $uibModalInstance.dismiss();
          }).catch(() => {
            toastr.error('Parola nu a fost resetată');
          });
        }
      });
    };

    $scope.validation = modal => {
      if (!modal.newPass) {
        toastr.error('Introduceţi parola nouă');
        return false;
      }
      if (!modal.checkPass) {
        toastr.error('Reintroduceţi parola nouă');
        return false;
      }
      if (modal.oldPass === modal.newPass) {
        toastr.error('Parola veche este identică cu parola nouă');
        return false;
      }
      if (modal.newPass !== modal.checkPass) {
        toastr.error('Parola de verificare nu coincide cu parola pe care doriţi să o setaţi');
        return false;
      }
      return true;
    };
  }
}());