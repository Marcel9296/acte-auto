(function () {
  'use strict';
  mainCtrl.$inject = ['$scope', '$uibModal', '$localStorage', '$location', '$rootScope', '$uibModalStack', 'toastr', '$loading', 'dialogs', 'regRefreshToken', 'regYears', 'utils'];
  angular.module('acte-auto').controller('mainCtrl', mainCtrl);
  function mainCtrl($scope, $uibModal, $localStorage, $location, $rootScope, $uibModalStack, toastr, $loading, dialogs, regRefreshToken, regYears, utils) {
    $localStorage.token = window.bootstrappedUser.token;
    $scope.role = window.bootstrappedUser.role;
    $scope.userName = window.bootstrappedUser.userName;
    $scope.idDemo = window.bootstrappedUser.id;
    $scope.id_unit = window.bootstrappedUser.id_unit;
    $scope.maxLeft = false;
    $scope.user = window.bootstrappedUser;


    $scope.startLoader = () => {
      $loading.start('loading-container');
    };
    $scope.stopLoader = () => {
      $loading.finish('loading-container');
    };

      $scope.changeLeftMenu = maxLeft => {
          $scope.maxLeft = !maxLeft;
          //$localStorage[window.bootstrappedUser.id].maxLeft = $scope.maxLeft;
          document.getElementById('left-menu').style.width = $scope.maxLeft ? '11%' : '3%';
          document.getElementById('bodyMain').style.width = $scope.maxLeft ? '89%' : '97%';
          utils.resizeContainer('left-menu', 3);
          $(window).trigger('resize');
      };


      $rootScope.$on('$locationChangeStart', () => {
      $uibModalStack.dismissAll('cancel');
    });

    window.onresize = () => {
      if ((window.outerHeight - window.innerHeight) <= 40) {
        document.getElementById('to-hide').style.display = 'block';
      } else {
        document.getElementById('to-hide').style.display = 'none';
      }
    };



    $scope.editProfile = () => {
      $uibModal.open({
        templateUrl: 'app/client/main/profile/profile-modal',
        controller: 'profileCtrl',
        backdrop: 'static',
        size: 'md',
        scope: $scope
      }).result.then(resp => {
          $scope.userName = resp.userName;
        }).catch(() => {
        });
    };

    $scope.resetPw = () => {
      $uibModal.open({
        templateUrl: 'app/client/main/resetPwdSelf/resetPwdSelf-modal',
        controller: 'resetPwdSelfCtrl',
        backdrop: 'static',
        size: 'md',
        scope: $scope
      }).result.catch(() => {
        });
    };

    $scope.logOut = () => {
      $localStorage.$reset();
      let a = document.createElement('a');
      document.body.appendChild(a);
      a.href = '/auth/logout';
      a.click();
    };

    function refreshToken() {
      regRefreshToken.refresh.get().$promise.then(resp => {
        delete $localStorage.farms;
        $localStorage.token = resp.token;
      });
    }

    setInterval(refreshToken, 30 * 60 * 1000);
  }
}());