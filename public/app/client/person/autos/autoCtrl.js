(function () {
  "use strict";
  autoCtrl.$inject = ['$scope', '$uibModal', '$uibModalInstance', '$location', '$localStorage', 'toastr', 'utils', 'regAuto', 'idAuto'];
  angular.module('acte-auto').controller('autoCtrl', autoCtrl);

  function autoCtrl($scope, $uibModal, $uibModalInstance, $location, $localStorage, toastr, utils, regAuto, idAuto) {
    $scope.auto = {id_person: $localStorage.idPerson};
    if(idAuto) {
      regAuto.byId.get({id: idAuto}).$promise.then(resp => {
        $scope.auto = resp;
      }).catch(() => toastr.error('Eroare la preluarea datelor'));
    }

    $scope.save = () => {
      if(idAuto){
        regAuto.simple.update($scope.auto).$promise.then(() => {
          toastr.success('Datele au fost salvate cu succes');
          $uibModalInstance.close();
        })
      } else {
        regAuto.simple.save($scope.auto).$promise.then(() => {
          toastr.success('Datele au fost salvate cu succes');
          $uibModalInstance.close();
        })
      }
    }
  }
}());