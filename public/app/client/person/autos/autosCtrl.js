(function () {
  "use strict";
  autosCtrl.$inject = ['$scope', '$uibModal', '$location', '$localStorage', 'toastr', 'regAuto', 'utils', 'dialogs'];
  angular.module('acte-auto').controller('autosCtrl', autosCtrl);

  function autosCtrl($scope, $uibModal, $location, $localStorage, toastr, regAuto, utils, dialogs) {
    $scope.changeLeftMenu(true);

    let loadData = () => {
      regAuto.byPerson.query({id_person: $localStorage.idPerson}).$promise.then(resp => {
        for(let i =0; i < resp.length; i++){
          if(resp[i].id_buyer){
            resp[i].sold = 'Da';
          } else {
            resp[i].sold = 'Nu';
          }
        }
        $scope.gridAutos.data = resp;
        utils.setHeight('grAutos', resp.length);
        $(window).trigger('resize');
      }).catch((err) => {
        toastr.error('A apărut o eroare');
      });
    };

    loadData();


    let edit = '<div class="text-center"><a class="btn btn-primary btn-xs m-top-4" ng-click="grid.appScope.editAutos(row.entity.id)" ng-disabled="grid.appScope.disableDelet" uib-tooltip="Editează vehicul"><i class="glyphicon glyphicon-edit"></i></a></div>',
      addBuyer = '<div class="text-center"><button class="btn btn-info btn-xs m-top-4" ng-disabled="grid.appScope.disableDelet" ng-click="grid.appScope.addBuyer(row.entity.id, row.entity.id_buyer)"><i class="glyphicon glyphicon-user"></i></button></div>',
      delet = '<div class="text-center"><button class="btn btn-danger btn-xs m-top-4" ng-disabled="grid.appScope.disableDelet" ng-click="grid.appScope.deleteAuto(row.entity)" uib-tooltip="Șterge autovehicul"><i class="glyphicon glyphicon-trash"></i></button></div>';
    $scope.gridAutos = {
      enableFiltering: true,
      enableRowSelection: true,
      enableSorting: true,
      enableRowHeaderSelection: false,
      multiSelect: false,
      showGridFooter: true,
      columnDefs: [
        {field: 'mark', displayName: 'Marca', minWidth: 60, cellClass: 'text-center', type: 'string', filter: {}},
        {field: 'manufacturing_year', displayName: 'Anul fabricației', minWidth: 60, maxWidth: 330, cellClass: 'text-center', type: 'string', filter: {}},
        {field: 'registration_number', displayName: 'Nr înmatriculare', minWidth: 60, maxWidth: 330, cellClass: 'text-center', type: 'string', filter: {}},
        {field: 'book_identification', displayName: 'Carte de identificare', minWidth: 60, maxWidth: 330, cellClass: 'text-center', type: 'string', filter: {}},
        {field: 'sold', displayName: 'Vândută', minWidth: 60, maxWidth: 330, cellClass: 'text-center', type: 'string', filter: {}},
        {field: 'edit', displayName: '', width: 30, enableFiltering: false, cellTemplate: edit, enableHiding: true},
        {field: 'addBuyer', displayName: '', width: 30, enableFiltering: false, cellTemplate: addBuyer, enableHiding: true},
        {field: 'destroy', displayName: '', width: 30, enableFiltering: false, cellTemplate: delet, enableHiding: true}
      ],
      rowTemplate: "<div ng-dblclick='grid.appScope.editAutos(row.entity)' ng-right-click='' ng-repeat='(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name' class='ui-grid-cell' ng-class=\"{\'text-red\' : row.entity.id_buyer}\" ui-grid-cell></div>"
    };

    $scope.addAutos = () => {
      $uibModal.open({
        templateUrl: 'app/client/person/autos/auto-modal',
        controller: 'autoCtrl',
        size: 'lg',
        scope: $scope,
        resolve: {
          idAuto: null
        }
      }).result.then(() => {
        loadData();
      }).catch(() => {
      });
    };

    $scope.editAutos = idAuto => {
      $uibModal.open({
        templateUrl: 'app/client/person/autos/auto-modal',
        controller: 'autoCtrl',
        size: 'lg',
        scope: $scope,
        resolve: {
          idAuto: idAuto
        }
      }).result.then(() => {
        loadData();
      }).catch(() => {
      });
    };

    $scope.addBuyer = (idAuto, idBuyer) => {
      $uibModal.open({
        templateUrl: 'app/client/person/autos/buyer/buyer-modal',
        controller: 'buyerCtrl',
        size: 'lg',
        scope: $scope,
        resolve: {
          idBuyer: (idBuyer ? idBuyer : null),
          idAuto: idAuto
        }
      }).result.then(() => {
        loadData();
      }).catch(() => {
      });
    };

    $scope.deleteAuto = (row) => {
      dialogs.confirm('Confirmă ştergerea', 'Doriţi să ştergeţi autovehiculul <b>' + row.mark + '</b> ?', {size: 'sm'}).result.then(() => {
        regAuto.byId.remove({id: row.id}).$promise.then(() => {
          $scope.gridAutos.data.splice($scope.gridAutos.data.indexOf(row), 1);
          toastr.success('Persoana ' + row.mark + ' a fost şters');
          utils.setHeight('grAutos', $scope.gridAutos.data.length);
          $(window).trigger('resize');
        }).catch((err) => {
          toastr.error('A apărut o eroare')
        });
      }, () => {
      });
    }

  }
}());