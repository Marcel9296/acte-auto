(function () {
  "use strict";
  buyerCtrl.$inject = ['$scope', '$uibModal', '$uibModalInstance', '$location', '$localStorage', 'utils', 'regCountry', 'regCounty', 'regLocality', 'regVillage', 'regPerson', 'idBuyer', 'idAuto', 'toastr'];
  angular.module('acte-auto').controller('buyerCtrl', buyerCtrl);

  function buyerCtrl($scope, $uibModal, $uibModalInstance, $location, $localStorage, utils, regCountry, regCounty, regLocality, regVillage, regPerson, idBuyer, idAuto, toastr) {
    $scope.buyer = {id_person: $localStorage.idPerson, address: {}, buyer: true, idAuto: idAuto};
    $scope.ob = {personType: [{name: 'Persoana Fizica', val: true}, {name: 'Persoana Juridica', val: false}], identityCard:['B.I.', 'C.I.'], firmType: ['S.R.L.', 'I.I.', 'I.F.', 'A.F.', 'P.F.A', 'C.M.I.']};
    $scope.locTypes = [{name: 'Municipiu', art: 'Municipiul', val: 1, ind: 0}, {name: 'Oraş',art: 'Oraşul',val: 2,ind: 0}, {name: 'Comună', art: 'Comuna', val: 3, ind: 1}];
    $scope.villageTypes = [{name: 'Localitate', art: 'Localitatea', val: 2}, {name: 'Sat', art: 'Satul', val: 3}];
    $scope.counties = $localStorage.counties;

    let loadData = () => {
      let t = [];

      if(idBuyer){
        t.push(cb => {
          regPerson.byId.get({id: idBuyer}).$promise.then(resp => {
            $scope.buyer = resp;
            cb();
          }).catch(err => cb(err));
        })
      }

      t.push(cb => {
        regCountry.simple.query().$promise.then(resp => {
          $scope.country = resp;
          cb();
        }).catch(err => cb(err));
      });

      t.push(cb => {
        regCounty.simple.query().$promise.then(resp => {
          $scope.county = resp;
          cb();
        }).catch(err => {
          cb(err)
        });
      });

      async.parallel(t, err => {
        if(err){
          toastr.error('Eroare la preluarea datelor!');
        } else {
          if (!idBuyer) {
            $scope.buyer.address.id_country = $scope.country[0].id;
            $scope.buyer.address.id_county = $scope.user.id_county;
            $scope.buyer.address.id_locality = $scope.user.id_locality;
            $scope.buyer.address.id_village = $scope.user.id_village;
            $scope.buyer.pf = $scope.ob.personType[0].val;
          }
          t = [];
          t.push(cb => {
            regLocality.byCounty.query({id_county: $scope.buyer.address.id_county}).$promise.then(resp => {
              $scope.ob.locality = resp;
              let loc = _.find(resp, l => l.id === $scope.buyer.address.id_locality);
              $scope.ob.typeLocality = loc.type;
              cb();
            }).catch(err => cb(err));
          });
          t.push(cb => {
            regVillage.byLocality.query({id_locality: $scope.buyer.address.id_locality}).$promise.then(resp => {
              $scope.ob.village = resp;
              cb();
            }).catch(err => cb(err));
          });

          async.parallel(t, err => {
            if (err) {
              toastr.error('Probleme la preluarea datelor');
            }
          });
        }
      });
    };

    loadData();

    $scope.changeCountry = utils.changeCountry;
    $scope.changeCounty = utils.changeCounty;
    $scope.changeLocality = utils.changeLocality;

    $scope.save = () => {
      if(idBuyer){
        regPerson.simple.update($scope.buyer).$promise.then(() => {
          toastr.success('Cumpărătorul a fost actualizat!');
          $uibModalInstance.close();
        })
      } else {
        regPerson.simple.save($scope.buyer).$promise.then(() => {
          toastr.success('Cumpărătorul a fost salvat cu success');
          $uibModalInstance.close();
        }).catch(() => toastr.error('Eroare la salvarea cumpărătorului'));
      }
    }
  }
}());