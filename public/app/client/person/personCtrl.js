(function () {
  "use strict";
  personCtrl.$inject = ['$scope', '$uibModal', '$localStorage', 'toastr', 'regPerson', 'regCountry', 'regCounty', 'utils', 'regLocality', 'regVillage'];
  angular.module('acte-auto').controller('personCtrl', personCtrl);

  function personCtrl($scope, $uibModal, $localStorage, toastr, regPerson, regCountry, regCounty, utils, regLocality, regVillage) {
    $scope.changeLeftMenu(true);
    $scope.ob = {personType: [{name: 'Persoana Fizica', val: true}, {name: 'Persoana Juridica', val: false}], identityCard:['B.I.', 'C.I.'], firmType: ['S.R.L.', 'I.I.', 'I.F.', 'A.F.', 'P.F.A', 'C.M.I.']};
    $scope.locTypes = [{name: 'Municipiu', art: 'Municipiul', val: 1, ind: 0}, {name: 'Oraş',art: 'Oraşul',val: 2,ind: 0}, {name: 'Comună', art: 'Comuna', val: 3, ind: 1}];
    $scope.villageTypes = [{name: 'Localitate', art: 'Localitatea', val: 2}, {name: 'Sat', art: 'Satul', val: 3}];
    $scope.counties = $localStorage.counties;

    let loadData = () => {
      let t = [];
      t.push(cb => {
        regPerson.byId.get({id: $localStorage.idPerson}).$promise.then(resp => {
          $scope.person = resp;
          $scope.person.address.type = resp.type;
          cb();
        }).catch(err => {
          cb(err);
        });
      });

      t.push(cb => {
        regCountry.simple.query().$promise.then(resp => {
          $scope.country = resp;
          cb();
        }).catch(err => cb(err));
      });

      t.push(cb => {
        regCounty.simple.query().$promise.then(resp => {
          $scope.county = resp;
          cb();
        }).catch(err => {
          cb(err)
        });
      });

      async.parallel(t, err => {
        if(err){
          toastr.error('Eroare la preluarea datelor!');
        } else {
          if (!$localStorage.idPerson) {
            $scope.person.address.id_country = $scope.country[0].id;
            $scope.person.address.id_county = $scope.user.id_county;
            $scope.person.address.id_locality = $scope.user.id_locality;
            $scope.person.address.id_village = $scope.user.id_village;
          }
          t = [];
          t.push(cb => {
            regLocality.byCounty.query({id_county: $scope.person.address.id_county}).$promise.then(resp => {
              $scope.ob.locality = resp;
              let loc = _.find(resp, l => l.id === $scope.person.address.id_locality);
              $scope.ob.typeLocality = loc.type;
              cb();
            }).catch(err => cb(err));
          });
          t.push(cb => {
            regVillage.byLocality.query({id_locality: $scope.person.address.id_locality}).$promise.then(resp => {
              $scope.ob.village = resp;
              cb();
            }).catch(err => cb(err));
          });

          async.parallel(t, err => {
            if (err) {
              toastr.error('Probleme la preluarea datelor');
            }
          });
        }
      });
    };

    loadData();

    $scope.changeCountry = utils.changeCountry;
    $scope.changeCounty = utils.changeCounty;
    $scope.changeLocality = utils.changeLocality;

    $scope.save = () => {
      regPerson.simple.update($scope.person).$promise.then(resp => {
        if(resp.success){
          loadData();
          toastr.success('Datele au fost salvate');
        }else {
          toastr.error('Probleme la salvarea datelor');
        }
      }).catch(err => toastr.error('Eroare la salvarea datelor!'));
    }
  }
}());