(function () {
  'use strict';
  angular.module('acte-auto').service('utils', ['$timeout', '$location', '$q', '$rootScope', 'toastr', '$localStorage', '$uibModal', 'regLocality', 'regVillage',
    function ($timeout, $location, $q, $rootScope, toastr, $localStorage, $uibModal, regLocality, regVillage) {

      this.changeCountry = address => {
        if (address.id_country === 1) {
          delete address.external_address;
        } else {
          delete address.id_county;
          delete address.id_locality;
          delete address.id_village;
          delete address.street;
          delete address.number;
          delete address.postal_code;
          delete address.block;
          delete address.floor;
          delete address.staircase;
          delete address.apartment;
        }
      };

      this.changeCounty = (address, ob) => {
        if (address.id_county) {
          regLocality.byCounty.query({id_county: address.id_county}).$promise.then(resp => {
            ob.locality = resp;
          }).catch(err => toastr.error('Probleme la preluarea localitatilor'));
        }
        delete address.id_locality;
        delete address.id_village;
        delete ob.village;
      };

      this.changeLocality = (address, ob) => {
        let loc = _.find(ob.locality, l => l.id === address.id_locality);
        ob.typeLocality = loc.type;
        ob.typeVillage = loc.type_village;
        delete address.id_village;
        if (address.id_locality) {
          regVillage.byLocality.query({id_locality: address.id_locality}).$promise.then(resp => {
            ob.village = resp;
          }).catch(err => {
            toastr.error('Probleme la preluarea localitatilor')
          });
        }
      };


      this.setId = (art, ob, id) => {
        art[id] = art && ob ? ob.id : null;
      };

      this.changeMaxLimit = (arr, limit, step) => {
        if (arr && limit.max < arr.length) {
          limit.max += step ? step : 2;
        }
      };

      this.resizeContainer = function(id, dif, atr) {
        let e = document.getElementById(id);
        if (e) {
          $timeout(() => {
            let distanceToTop = e.getBoundingClientRect().top + (dif ? dif : 20);
            let vh = window.innerHeight;
            if (vh > 450) {
                e.style[atr ? atr : 'height'] = vh - distanceToTop + 'px';
            }
          });
        }
      };

      this.changeBeginLimit = (arr, limit, displayed, step) => {
        // displayed - number of displayed rows (first parameter of limitTo)
        displayed = displayed ? displayed : 40;
        // step - step for increment begin (second parameter of limitTo)
        step = step ? step : 2;
        if (arr && limit.begin <= arr.length - displayed) {
          limit.begin += step;
        }
      };

      this.openCertificates = $scope => {
        if (!$scope.disabledAdd) {
          $scope.disabledAdd = true;
          $uibModal.open({
            templateUrl: 'app/client/farm/certificates/certificate-modal',
            controller: 'certificatesCtrl',
            size: 'lg',
            scope: $scope
          }).result.catch(() => {
              $scope.disabledAdd = false;
            });
        }
      };

      this.multiSave = farm => {
        return $q((resolve) => {
          let tasks = [];
          for (let cap in farm) {
            tasks.push(cb => {
              farm[cap].reg.simple.update(farm[cap].obj).$promise.then(() => {
                cb();
              }).catch(err => cb(err));
            });
          }

          async.parallel(tasks, err => {
            if (err) {
              $localStorage.errorAction = 'Salvare multipla';
              $location.path('/contact/');
            } else {
              resolve(true);
            }
          });

        });
      };

      this.open = ($event, op, opened) => {
        let val = opened[op];
        opened = [];
        $event.preventDefault();
        $event.stopPropagation();
        opened[op] = !val;
        return opened;
      };

      //this.setHeight = (id, lng) => {
      //  if (lng < 20) {
      //    let el = document.getElementById(id);
      //    if (el) {
      //      el.style.height = (lng * 30 + 130) + 'px';
      //      $timeout(() => {
      //        $(window).trigger('resize');
      //      });
      //    }
      //  }
      //};

      this.setHeight = (id, lng, bottom) => {
        let e = document.getElementById(id);
        if(e) {
          let distanceToTop = e.getBoundingClientRect().top + 15;
          let height = window.innerHeight - distanceToTop - (bottom ? bottom : 0);
          let rowsHeight = lng * 30 + 110;
          e.style.height = (rowsHeight < height ? rowsHeight : height) + 'px';
        }
      };

      this.replaceDiacritics = text => {
        return text ? text.toLowerCase().normalize('NFKD').replace(/[^\w]/g, '') : null;
      };

      this.roFilter = (searchTerm, cellValue) => {
        if (!cellValue) {
          return false;
        }
        return this.replaceDiacritics(cellValue.toString()).indexOf(this.replaceDiacritics(searchTerm)) > -1;
      };

      this.replaceDiacritice = (text) => {
        text = text.replace(new RegExp('ă', 'g'), 'a');
        text = text.replace(new RegExp('Ă', 'g'), 'A');
        text = text.replace(new RegExp('â', 'g'), 'a');
        text = text.replace(new RegExp('Â', 'g'), 'A');
        text = text.replace(new RegExp('î', 'g'), 'i');
        text = text.replace(new RegExp('Î', 'g'), 'I');
        text = text.replace(new RegExp('ş', 'g'), 's');
        text = text.replace(new RegExp('ș', 'g'), 's');
        text = text.replace(new RegExp('Ş', 'g'), 'S');
        text = text.replace(new RegExp('Ș', 'g'), 'S');
        text = text.replace(new RegExp('ț', 'g'), 't');
        text = text.replace(new RegExp('ţ', 'g'), 't');
        text = text.replace(new RegExp('Ţ', 'g'), 'T');
        return text;
      };

      this.diacriticsFilter = (searchTerm, cellValue) => {
        if (!cellValue) {
          return false;
        }
        let tokens = searchTerm.split(' ');
        let result = true;
        for (let i = 0; i < tokens.length; i++) {
          result = result && this.replaceDiacritice(cellValue.toLowerCase()).indexOf(this.replaceDiacritice(tokens[i].toLowerCase())) >= 0;
        }
        return result || this.replaceDiacritice(cellValue.toLowerCase()).indexOf(this.replaceDiacritice(searchTerm.toLowerCase())) >= 0;
      };

      /* --------------------------------------- VALIDARI, CAUTARE COMPANII --------------------------------------- */

      let key = 'LCf2Et1Qqraw1mztx4ozaXdpHELvZ8YCx7-2FspDhzhhNvjZaw';

      this.checkCnp = cnp => {
        if (cnp && cnp.length === 13) {
          let months = [], counties = [],
            control = [2, 7, 9, 1, 4, 6, 3, 5, 8, 2, 7, 9],
            days = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
          for (let i = 1; i < 47; i++) {
            if (i < 10) {
              counties.push('0' + i);
              months.push('0' + i);
            } else {
              counties.push('' + i);
              if (i < 13) {
                months.push('' + i);
              }
            }
          }
          counties.push('51');
          counties.push('52');
          let c = [];
          for (let i = 0, ln = cnp.length; i < ln; i++) {
            c.push(parseInt(cnp[i]));
          }
          if (c[0] < 1 || c[0] > 8) {
            //toastr.error('Prima cifră invalidă');
            toastr.error('CNP-ul introdus nu este valid');
            return false;
          }
          let ind = months.indexOf(cnp.substring(3, 5));
          if (ind === -1) {
            //toastr.error('Luna invalidă');
            toastr.error('CNP-ul introdus nu este valid');
            return false;
          } else {
            let d = days[ind];
            if (parseInt(cnp.substring(1, 3)) % 4 === 0 && ind === 1) {
              d = 29;
            }
            if (parseInt(cnp.substring(5, 7)) > d) {
              //toastr.error('Ziua invalidă');
              toastr.error('CNP-ul introdus nu este valid');
              return false;
            }
          }
          //if (counties.indexOf(cnp.substring(7, 9)) === -1) {
          //  //toastr.error('Codul județului invalid');
          //  toastr.warning('CNP-ul introdus nu este valid');
          //  return false;
          //}
          let sum = 0;
          for (let i = 0, ln = control.length; i < ln; i++) {
            sum += c[i] * control[i];
          }
          let controlNumber = (sum % 11) === 10 ? 1 : sum % 11;
          if (controlNumber === c[12]) {
            //toastr.success('CNP valid');
            return true;
          } else {
            //toastr.error('CNP invalid');
            toastr.error('CNP-ul introdus nu este valid');
            return false;
          }
        } else {
          return false;
        }
      };

      this.checkCif = cui => {
        if (cui && cui.length > 3 && cui.length < 11) {
          let control = [2, 3, 5, 7, 1, 2, 3, 5, 7], controlNumber = parseInt(cui[cui.length - 1]), c = [], sum = 0;
          for (let i = cui.length - 2; i >= 0; i--) {
            c.push(parseInt(cui[i]));
          }
          for (let i = 0, ln = c.length; i < ln; i++) {
            sum += c[i] * control[i];
          }
          sum *= 10;
          sum = sum % 11;
          sum = sum === 10 ? 0 : sum;
          if (sum === controlNumber) {
            //toastr.success('CUI valid');
            return true;
          } else {
            //toastr.error('CUI invalid');
            toastr.warning('Codul fiscal introdus nu este valid');
            return false;
          }
        } else {
          //toastr.error('CUI-ul trebuie sa conțină între 4 și 9 numere');
          return false;
        }
      };

      this.getCompany = (cif) => {
        return $q((resolve) => {
          let url = 'https://api.openapi.ro/api/companies/' + cif;
          $.ajax({
            url: url, beforeSend: (xhr) => {
              xhr.setRequestHeader('x-api-key', key);
            }, success: (resp) => {
              if (resp.judet) {
                resp.judet = resp.judet.replace('Municipiul', '').trim();
              }
              resolve(resp);
            }
          });
        });
      };

      /* --------------------------------------- IMPORT PROGRAM --------------------------------------- */

      this.moveElemFirst = (arr, col, val) => {
        if (arr.length && col && val) {
          let ind;
          for (let i = 0, lng = arr.length; i < lng; i++) {
            if (arr[i][col] === val) {
              ind = i;
              break;
            }
          }
          let element = arr[ind];
          arr.splice(ind, 1);
          arr.splice(0, 0, element);
          return arr;
        }
        return arr;
      };

      this.replaceForPdf = (text) => {
        let re = new RegExp('\n', 'g');
        let ret = new RegExp('\t', 'g');
        if (text && text.length > 0) {
          text = text.replace(ret, '&#8195 ');
          return '<p class="indentRepl">' + text.replace(re, '</p><p class="indentRepl">') + '</p>';
        }
        return text;
      };

      this.getLocalityType = function (type) {
        if (type == 1) {
          return 'Municipiul';
        } else if (type == 2) {
          return 'Orașul';
        } else {
          return 'Comuna';
        }
      };

      this.findScrollDirection = (e, scope)=>{
        let delta = e.wheelDelta;
        if (delta >= 0) {
          scope.limit.begin = scope.limit.begin >= 8 ? scope.limit.begin-8 : 0;
          if (!(scope.$$phase || $rootScope.$$phase)) {
            scope.$digest();
          }
        }
      };

      this.keyDown = (e, scope, lng) =>{
        if (e.keyCode === 33) {
          // PageUp
          scope.limit.begin = scope.limit.begin >= 6 ? scope.limit.begin-6 : 0;
          if (!(scope.$$phase || $rootScope.$$phase)) {
            scope.$digest();
          }
        } else if (e.keyCode === 34) {
          // PageDown
          if (lng && scope.limit.begin <= lng - 30) {
            scope.limit.begin += 6;
            if (!(scope.$$phase || $rootScope.$$phase)) {
              scope.$digest();
            }
          }
        } else if (e.keyCode === 38) {
          // KeyUp
          scope.limit.begin = scope.limit.begin >= 2 ? scope.limit.begin-2 : 0;
          if (!(scope.$$phase || $rootScope.$$phase)) {
            scope.$digest();
          }
        } else if (e.keyCode === 40) {
          // KeyDown
          if (lng && scope.limit.begin <= lng - 30) {
            scope.limit.begin += 2;
            if (!(scope.$$phase || $rootScope.$$phase)) {
              scope.$digest();
            }
          }
        } else if (e.keyCode === 27) {
          // Escape - scroll top
          if (lng) {
            scope.limit.begin = 0;
            this.scrollTop('view-units');
            if (!(scope.$$phase || $rootScope.$$phase)) {
              scope.$digest();
            }
          }
        }
      };
    }]);
})();