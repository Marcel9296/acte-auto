(function () {
    'use strict';
    angular.module('acte-auto').service('functions', [function () {

        this.makeOperation = (fm, definitions, def, sLine, columns) => {
            //let total = {ha: 0, ari: 0, area: 0, more_ha: 0, more_ari: 0, val1: 0, val2: 0, area_az: 0, amount_az: 0, area_fo: 0, amount_fo: 0, area_po: 0, amount_po: 0, totalArea: 0, totalAmount: 0};
            let total = {};
            for (let i = 0; i < columns.length; i++) {
                total[columns[i]] = 0;
            }
            for (let j = 0; j < fm.length; j++) {
                if (!isNaN(fm[j])) {
                    let df = definitions.filter((f) => {
                        return f.code_row === parseInt(fm[j]);
                    });
                    if (df.length) {
                        for (let col in total) {
                            if (total.hasOwnProperty(col) && df[0].hasOwnProperty(col)) {
                                total[col] = eval((fm[j - 1] ? total[col] + fm[j - 1] : '') + (df[0][col] ? parseFloat(df[0][col]) : 0));
                            }
                        }
                    }
                }
            }

            total.ha += Math.floor(total.ari / 100);
            total.ari = total.ari ? (total.ari % 100).toFixed(2) : 0;
            total.more_ha += Math.floor(total.more_ari / 100);
            total.more_ari = total.more_ari ? (total.more_ari % 100).toFixed(2) : 0;
            total.more_ha = total.more_ha ? total.more_ha : 0;
            total.ha = total.ha ? total.ha : 0;
            total.area = total.area ? total.area : 0;
            total.val1 = total.val1 ? total.val1 : null;
            total.val2 = total.val2 ? total.val2 : null;
            // pt cap 10b
            total.area_az = total.area_az ? total.area_az.toFixed(2) : null;
            total.amount_az = total.amount_az ? total.amount_az.toFixed(2) : null;
            total.area_fo = total.area_fo ? total.area_fo.toFixed(2) : null;
            total.amount_fo = total.amount_fo ? total.amount_fo.toFixed(2) : null;
            total.area_po = total.area_po ? total.area_po.toFixed(2) : null;
            total.amount_po = total.amount_po ? total.amount_po.toFixed(2) : null;
            return this.setVal(def, total, sLine);
        };

        this.setVal = (def, total, sLine) => {
            def.ha = total.ha;
            if (total.ari < 0) {
                total.ari = (100 + parseFloat(total.ari)).toFixed(2);
            }
            def.ari = total.ari;
            def.more_ha = total.more_ha;
            def.more_ari = total.more_ari;
            def.area = total.area;
            def.val1 = total.val1;
            def.val2 = total.val2;
            // pt cap 10b
            def.area_az = total.area_az;
            def.amount_az = total.amount_az;
            def.area_fo = total.area_fo;
            def.amount_fo = total.amount_fo;
            def.area_po = total.area_po;
            def.amount_po = total.amount_po;
            return sLine ? this.sumLine(def) : def;
        };

        this.sumLine = (def) => {
            let mari = def.more_ari ? parseFloat(def.more_ari) : 0;
            let tari = (def.ari ? parseFloat(def.ari) : 0) + (def.more_ari ? mari : 0);
            def.ha = !def.ha && def.ari ? 0 : def.ha;
            def.more_ha = !def.more_ha && def.more_ari ? 0 : def.more_ha;
            def.totalHa = (def.ha ? parseInt(def.ha) : 0) + (def.more_ha ? parseInt(def.more_ha) : 0) + (tari / 100 | 0);
            def.totalAri = (tari % 100).toFixed(2);
            // pt cap 10b
            def.totalArea = (parseFloat(def.area_az ? def.area_az : 0) + parseFloat(def.area_fo ? def.area_fo : 0) + parseFloat(def.area_po ? def.area_po : 0)).toFixed(2);
            def.totalAmount = (parseFloat(def.amount_az ? def.amount_az : 0) + parseFloat(def.amount_fo ? def.amount_fo : 0) + parseFloat(def.amount_po ? def.amount_po : 0)).toFixed(2);
            return this.checkNull(def);
        };

        this.checkNull = (d) => {
            if (!d.totalHa && !parseFloat(d.totalAri)) {
                d.ha = null;
                d.ari = null;
                d.more_ha = null;
                d.more_ari = null;
                d.totalHa = null;
                d.totalAri = null;
            }
            // pt cap 10b
            if (!parseFloat(d.totalArea) && !parseFloat(d.totalAmount)) {
                d.area_az = null;
                d.amount_az = null;
                d.area_fo = null;
                d.amount_fo = null;
                d.area_po = null;
                d.amount_po = null;
                d.totalArea = null;
                d.totalAmount = null;
            }
            return d;
        };

    }]);
}());
