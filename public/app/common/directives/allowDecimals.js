(function () {
  'use strict';
  angular.module('acte-auto').directive('allowDecimals', () => {
    return {
      require: 'ngModel',
      link: (scope, element, attrs, modelCtrl) => {
        let value;
        modelCtrl.$parsers.push(inputValue => {
          if (inputValue.toString().length > 0) {
            let transformedInput, re, decimals = parseInt(attrs.allowDecimals);
            if (decimals && !isNaN(decimals) && decimals < 7) {
              re = new RegExp('^[-]?((\\d+)\\.?(\\d{1,'+attrs.allowDecimals+'})?)', 'g');
            } else {
              re = new RegExp(/((\d+)\.?(\d{1,4})?)/g);
            }
            transformedInput = inputValue.toString().match(re);
            if (transformedInput === null) {
              transformedInput = [''];
            }
            if (transformedInput && transformedInput[0] !== inputValue) {
              modelCtrl.$setViewValue(transformedInput[0]);
              modelCtrl.$render();
            }
            if (transformedInput) {
              return transformedInput[0];
            }
            value = '';
            return '';
          } else {
            value = null;
            return null;
          }
        });
        element.on('blur', () => {
          if (value && value[value.length - 1] === '.') {
            modelCtrl.$setViewValue(value.substring(0, value.length - 1));
            modelCtrl.$render();
          }
        });
      }
    };
  });
})();