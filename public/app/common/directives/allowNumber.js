(function () {
  'use strict';
  angular.module('acte-auto').directive('allowNumber', function () {
    return {
      require: 'ngModel',
      link: function (scope, element, attrs, modelCtrl) {
        modelCtrl.$parsers.push(function (inputValue) {
          if (inputValue.toString().length > 0) {
            let transformedInput = inputValue.toString().match(/(\d+)/g);
            //var transformedInput = inputValue.toString().match('\b0*([1-9][0-9]*|0)\b');
            if (transformedInput === null) {
              transformedInput = [''];
            }
            if (transformedInput && transformedInput[0] !== inputValue) {
              modelCtrl.$setViewValue(transformedInput[0]);
              modelCtrl.$render();
            }
            if (transformedInput && transformedInput[0] === '') {
              return null;
            }
            if (transformedInput) {
              if (attrs.allowNumber === 'integer') {
                return parseInt(transformedInput[0]);
              } else {
                return transformedInput[0];
              }
            }
            return null;
          } else {
            return null;
          }
        });
      }
    };
  });
})();
