(function () {
    'use strict';
    let gulp = require('gulp'),
        babel = require('gulp-babel'),
        plumber = require('gulp-plumber'),
        uglify = require('gulp-uglify'),
        concat = require('gulp-concat'),
        cssmin = require('gulp-cssmin'),
        less = require('gulp-less');

// Scripts Task - dev
    gulp.task('scripts-admin-dev', function () {
        return gulp.src('public/app/admin/**/*.js')
            .pipe(require('gulp-jshint')())
            .pipe(concat('adminApp.min.js'))
            .pipe(gulp.dest('./public/dist'))
            .pipe(require('gulp-livereload')());
    });

    gulp.task('scripts-client-dev', function () {
        return gulp.src('public/app/client/**/*.js')
            .pipe(require('gulp-jshint')())
            .pipe(concat('cliApp.min.js'))
            .pipe(gulp.dest('./public/dist'))
            .pipe(require('gulp-livereload')());
    });

    gulp.task('services-dev', function () {
        return gulp.src('public/app/common/**/*.js')
            .pipe(require('gulp-jshint')())
            .pipe(concat('service.min.js'))
            .pipe(gulp.dest('./public/dist'))
            .pipe(require('gulp-livereload')());
    });

    gulp.task('login-less-dev', function () {
        return gulp.src('public/app/less/login.less')
            .pipe(less())
            .pipe(cssmin())
            .pipe(gulp.dest('./public/dist/css'))
            .pipe(require('gulp-livereload')());
    });

    gulp.task('less-dev', function () {
        return gulp.src(['public/app/less/**/*.less', '!public/app/less/login.less', '!public/app/less/registru.less'])
            .pipe(plumber())
            .pipe(less())
            .pipe(cssmin())
            .pipe(concat('cliApp.min.css'))
            .pipe(gulp.dest('./public/dist/css'))
            .pipe(plumber.stop())
            .pipe(require('gulp-livereload')());
    });

// Scripts Task - production
    gulp.task('scripts-admin', function () {
        return gulp.src('public/app/admin/**/*.js')
            .pipe(babel({presets: ['es2015']}))
            .pipe(uglify({mangle: false}))
            .pipe(concat('adminApp.min.js'))
            .pipe(gulp.dest('./public/dist'));
    });

    gulp.task('scripts-client', function () {
        return gulp.src('public/app/client/**/*.js')
            .pipe(babel({presets: ['es2015']}))
            .pipe(uglify({mangle: false}))
            .pipe(concat('cliApp.min.js'))
            .pipe(gulp.dest('./public/dist'));
    });

    gulp.task('services', function () {
        return gulp.src('public/app/common/**/*.js')
            .pipe(babel({presets: ['es2015']}))
            .pipe(uglify({mangle: false}))
            .pipe(concat('service.min.js'))
            .pipe(gulp.dest('./public/dist'));
    });

    gulp.task('login-less', function () {
        return gulp.src('public/app/less/login.less')
            .pipe(less())
            .pipe(cssmin())
            .pipe(gulp.dest('./public/dist/css'));
    });

    gulp.task('registru-less', function () {
        return gulp.src('public/app/less/registru.less')
            .pipe(less())
            .pipe(cssmin())
            .pipe(gulp.dest('./public/dist/css'));
    });

    gulp.task('less', function () {
        return gulp.src(['public/app/less/**/*.less', '!public/app/less/login.less', '!public/app/less/registru.less'])
            .pipe(plumber())
            .pipe(less())
            .pipe(cssmin())
            .pipe(concat('cliApp.min.css'))
            .pipe(gulp.dest('./public/dist/css'))
            .pipe(plumber.stop());
    });
//// pug Task
    gulp.task('pug', function () {
        return gulp.src('public/app/**/*.pug');
    });


//// clean Task
    gulp.task('clean', function () {
        return gulp.src('./public/dist/*')
            .pipe(require('gulp-clean')());
    });

    gulp.task('watch', function () {
        require('gulp-livereload').listen();
        gulp.watch('public/app/admin/**/*.js', ['scripts-admin-dev']);
        gulp.watch('public/app/client/**/*.js', ['scripts-client-dev']);
        gulp.watch('public/app/common/**/*.js', ['services-dev']);
        gulp.watch(['public/app/less/**/*.less', '!public/app/less/login.less'], ['less']);
        gulp.watch('public/app/less/login.less', ['login-less']);
        gulp.watch('public/app/less/registru.less', ['registru-less']);
        gulp.watch('public/app/**/*.pug', ['pug']).on('change', function (file) {
            gulp.src(file.path)
                .pipe(require('gulp-livereload')());
        });
    });

    gulp.task('build', function (cb) {
        require('run-sequence')('clean', ['login-less', 'registru-less', 'less', 'scripts-admin-dev', 'scripts-client-dev', 'services-dev', 'pug'], 'watch', cb);
    });

    gulp.task('default', ['build']);
    gulp.task('heroku:staging', ['login-less', 'registru-less', 'less', 'scripts-admin', 'scripts-client', 'services']);
    gulp.task('heroku:production', ['login-less', 'less', 'scripts-admin', 'scripts-client', 'services']);
}());